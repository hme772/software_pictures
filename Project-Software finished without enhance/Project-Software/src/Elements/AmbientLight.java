package Elements;

import java.awt.Color;
//import java.util.Objects;

import Primitives.Point3D;
import Primitives.Vector;

public class AmbientLight extends Light
{//ambient light is the light that there is everywhere
	private double _ka/*=0.1*/;
	
	//constructor

	public AmbientLight(Color _color, double _ka1) {
		super(_color);
		this._ka = _ka1;
	}
	//copy constructor
	public AmbientLight(AmbientLight a)
	{
		super(a._color);
		this._ka=a._ka;
	}
	
	//gets and sets values
	public double get_ka() {
		return _ka;
	}


	public void set_ka(double _ka1) {
		this._ka = _ka1;
	}

	
	//administration
	@Override
	public String toString() {
		return "AmbientLight [_ka=" + _ka + "]";
	}


	@Override
	public boolean equals(Object obj) 
	{
	//checks if 2 ambient lights are the same one
		AmbientLight a=(AmbientLight)obj;
		return (a._color==this._color && a._ka==this._ka);
	}

	//operations
	public Color getIntensity(Point3D point)
	{
		return new Color((int)Math.round(_color.getRed()*_ka), (int)Math.round( _color.getGreen()*_ka), (int)Math.round( _color.getBlue()*_ka));
	}
	
	///empty implementation because there is no need for a function to return a vector from a point to the light (it's everywhere)
	@Override
	public Vector getL(Point3D point) {
		// TODO Auto-generated method stub
		return null;
	}


}
