package Elements;

import java.awt.Color;
//import java.util.Objects;

import Primitives.Point3D;
import Primitives.Vector;

public class DirectionalLight extends Light
{
	//light like the sun, stands in the infinity and only has a direction

	Vector _direction;
	
	//constructor
	public DirectionalLight(Color _color, Vector _direction) {
		super(_color);
		this._direction = _direction;
	}

	//gets and sets values

	public Vector get_direction() {
		return new Vector( _direction);
	}

	public void set_direction(Vector _direction) {
		this._direction =new Vector( _direction);
	}
	
	//administration
	@Override
	public String toString() {
		return "DirectionalLight [_direction=" + _direction + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		
		DirectionalLight l=(DirectionalLight)obj;
		return (l._direction.equals(this._direction)&& l._color==this._color);
	}

	//operations
	@Override
	public Color getIntensity(Point3D point) {
		//returns the color in that spot
		// TODO Auto-generated method stub
		return new Color(_color.getRed(),_color.getGreen(),_color.getBlue());
	}

	///empty implementation because this light is in the infinity
	@Override
	public Vector getL(Point3D point) {
		// TODO Auto-generated method stub
		return null;
	}

}
