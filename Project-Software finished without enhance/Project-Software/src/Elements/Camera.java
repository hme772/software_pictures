package Elements;
//import java.util.Objects;

import Primitives.*;
public class Camera 
{
	//creates a camera

	Point3D _P0;
	Vector _vUp;//y
	Vector _vTo;//z that goes by negative values as in front of the camera
	Vector _vRight;//x
	
	//constructor
	public Camera( Vector _vUp, Vector _vTo, Vector _vRight) {
		super();
		this._P0 = new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
		this._vUp = _vUp;
		this._vTo = _vTo;
		this._vRight = _vRight;
	}
	
	//copy constructor

	public Camera(Camera c)
	{
		this._P0=c._P0;
		this._vUp = c._vUp;
		this._vTo = c._vTo;
		this._vRight = c._vRight;
		
	}

	//gets and sets values
	public Point3D get_P0() {
		return _P0;
	}

	public void set_P0(Point3D _P0) {
		this._P0 = _P0;
	}

	public Vector get_vUp() {
		return _vUp;
	}

	public void set_vUp(Vector _vUp) {
		this._vUp = _vUp;
	}

	public Vector get_vTo() {
		return _vTo;
	}

	public void set_vTo(Vector _vTo) {
		this._vTo = _vTo;
	}

	public Vector get_vRight() {
		return _vRight;
	}

	public void set_vRight(Vector _vRight) {
		this._vRight = _vRight;
	}
	//administration
	@Override
	public String toString() {
		return "Camera [_P0=" + _P0 + ", _vUp=" + _vUp + ", _vTo=" + _vTo + ", _vRight=" + _vRight + "]";
	}

	@Override
	public boolean equals(Object obj) {
		
		Camera c=(Camera)obj;
		return c._vRight.equals(get_vRight()) && c._vTo.equals(get_vTo()) && c._vUp.equals(get_vUp()) &&c._P0.equals(get_P0());
		
	}
		
	//operations
	
	public Ray constructRayThroughPixel (int Nx, int Ny, double x, double y, double screenDist, double screenWidth,double screenHeight) 
	{//this function construct a ray through a pixel according the formula that was given in class 
		//temporary calculating
		Point3D P0 = new Point3D(this._P0);
		//double d= 100;
		Vector Vin=new Vector(this._vTo);
		Vin.scale(screenDist);
		P0.add(Vin);
		Point3D PC= P0;//reference *not* value!!!
		double Rx,Ry;
		Rx=screenWidth/(double)Nx;
		Ry=screenHeight/(double)Ny;
	    //right calculating
		Vector Vright=new Vector(this._vRight);
		double X=(x-((double)Nx/2))*Rx-(Rx/2);
		Vright.scale(X);
		PC.add(Vright);
	    //up calculating
		Vector Vup=new Vector(this._vUp);
		double Y=(y-((double)Ny/2))*Ry-(Ry/2);
		Vup.scale(Y);

		PC.subtract(Vup);//the final dot
		Vector V=new Vector(new Point3D(PC.getX(),PC.getY(),PC.getZ()));
		V.normalize();
		Point3D SomeP0 = new Point3D(this._P0);

		Ray finaly=new Ray(SomeP0,V);
		return finaly;

	}

	
}
