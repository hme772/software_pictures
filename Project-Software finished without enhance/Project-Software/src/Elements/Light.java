package Elements;

import java.awt.Color;

import Primitives.Point3D;
import Primitives.Vector;

public abstract class Light 
{
	//an abstruct class to define light, all types of light has a color
	
	Color  _color;
	//constructor
	
	public Light(Color _color) {
		super();
		this._color = _color;
	}
	
	//operations
	abstract public Color getIntensity(Point3D point);//returns the intensity of the light(which is the color)
	abstract public Vector getL(Point3D point);//returns the vector from the light to the given point
}
