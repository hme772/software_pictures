package Elements;

import java.awt.Color;
//import java.util.Objects;

import Primitives.Point3D;
import Primitives.Vector;

public class PointLight extends Light
{// a light that has 3 consts and position

	public Point3D _position;
	public double _kc;
	public double _kl;
	public double _kq;
	
	//constructor	
	public PointLight(Color _color, Point3D _position, double _kc, double _kl, double _kq) {
		super(_color);
		this._position = _position;
		this._kc = _kc;
		this._kl = _kl;
		this._kq = _kq;
	}
	
	
	
	//gets and sets values
	
	public Point3D get_position() {
		return new Point3D(_position);
	}

	public void set_position(Point3D _position) {
		this._position = new Point3D(_position);
	}

	public double get_kc() {
		return _kc;
	}

	public void set_kc(double _kc) {
		this._kc = _kc;
	}

	public double get_kl() {
		return _kl;
	}

	public void set_kl(double _kl) {
		this._kl = _kl;
	}

	public double get_kq() {
		return _kq;
	}

	public void set_kq(double _kq) {
		this._kq = _kq;
	}
	
	//administration

	@Override
	public String toString() {
		return "PointLight [_position=" + _position + ", _kc=" + _kc + ", _kl=" + _kl + ", _kq=" + _kq + "]";
	}



	@Override
	public boolean equals(Object obj) {
		
		PointLight p =(PointLight)obj;
		return (p._kc==this._kc && p._kl==this._kl && p._kq==this._kq &&  p._position.equals(this._position));
	}



	//operations
	@Override
	public Color getIntensity(Point3D point) {
		//calculates the color at this point by the given formula we've learned in class
		double d=_position.distance(point);
		double result=_kc+_kl*d+_kq*d*d;
		
		int red= (int)(_color.getRed()/result);
		int green = (int)( _color.getGreen()/result);
		int blue = (int)( _color.getBlue()/result);
		
		//checks that the color is within the range(will not cross 255 in each parameter)
		if(red>255)red=255;
		if(green>255)green=255;
		if(blue>255)blue=255;
		
		Color I0 = new Color (red,green,blue);
		return I0;
	}

	
	@Override
	public Vector getL(Point3D point)
	{
	//a normalized vector from the light to the point
		Vector L=new Vector(_position);
		L.subtract(new Vector(point));
		L.scale(-1);
		L.normalize();
		return L;
	}

}
