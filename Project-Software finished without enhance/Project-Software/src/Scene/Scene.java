package Scene;

import java.awt.Color;
import java.util.List;


//import java.util.Objects;
import java.util.ArrayList;
import java.util.Iterator;

import Elements.AmbientLight;

//import java.util.Vector;

import Elements.Camera;
import Elements.Light;
import Geometries.*;
import Primitives.Coordinate;
import Primitives.Point3D;
import Primitives.Vector;

public class Scene 
{

	//creates a scene
	
	String _sceneName;
	Color _background;
    AmbientLight _ambientLight;
	ArrayList<Geometry> _geometries;
	List<Light> _lights;
	Camera _camera;
	double _screenDistance;
	
	//constructor
	public Scene(String _sceneName, Color _background, AmbientLight  _ambientLight, ArrayList<Geometry> _geometries,List<Light> _lights, Camera _camera,
			double _screenDistance) {
		super();
		this._sceneName = _sceneName;
		this._background = _background;
		this._ambientLight=_ambientLight;
		this._geometries = _geometries;
		this._lights=_lights;
		this._camera = _camera;
		this._screenDistance = _screenDistance;
	}

	//default constructor
	public Scene() {
		super();
		this._sceneName = "";
		this._background = Color.BLACK;
		this._geometries =  new ArrayList<Geometry>();
		this._ambientLight= new AmbientLight(Color.WHITE,0.0);
		this._lights=new ArrayList<Light>();
		
		Vector Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		Vector Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Vector Vto=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));

		this._camera = new Camera(Vup,Vto,Vright);
		this._screenDistance = 150;
	}
	
	//copy constructor
	public Scene(Scene s) {
		super();
		this._sceneName = s._sceneName;
		this._background = s._background;
		this._ambientLight=s._ambientLight;
		this._geometries = s._geometries;
		this._lights=s._lights;
		this._camera = s._camera;
		this._screenDistance = s._screenDistance;
	}
	
	
	
	//gets and sets values
	public String get_sceneName() {
		return _sceneName;
	}

	public void set_sceneName(String _sceneName) {
		this._sceneName = _sceneName;
	}

	public Color get_background() {
		return _background;
	}

	public void set_background(Color _background) {
		this._background = _background;
	}

	public List<Geometry> get_geometries() {
		return new ArrayList<Geometry>(_geometries);
	}

	public void set_geometries(List<Geometry> _geometries) {
		this._geometries = new ArrayList<Geometry>(_geometries);
	}

	public Camera get_camera() {
		return _camera;
	}

	public void set_camera(Camera _camera) {
		this._camera =_camera;
	}

	public double get_screenDistance() {
		return _screenDistance;
	}

	public void set_screenDistance(double _screenDistance) {
		this._screenDistance = _screenDistance;
	}
	
	public AmbientLight get_ambientLight() {
		return new AmbientLight(_ambientLight);
	}

	public void set_ambientLight(AmbientLight _ambientLight) {
		this._ambientLight = new AmbientLight(_ambientLight);
	}

	public List<Light> get_lights() {
		return new ArrayList<Light>(_lights);
	}

	public void set_lights(List<Light> _lights) {
		this._lights = new ArrayList<Light>(_lights);
	}

	
	//administration
	@Override
	public boolean equals(Object obj) {
		//checks if 2 scenes are the same 1
		Scene s=(Scene)obj;
		return (s.get_background().equals(this.get_background())&&
				s.get_camera().equals(this.get_camera())&&
				s.get_ambientLight().equals(this._ambientLight)&&
				s.get_geometries().equals(this.get_geometries())&&
				s.get_lights().equals(this._lights)&&
				s.get_screenDistance()==this.get_screenDistance());
			}
	
	@Override
	public String toString() {
		return "Scene [_sceneName=" + _sceneName + ", _background=" + _background + ", _ambientLight=" + _ambientLight
				+ ", _geometries=" + _geometries + ", _lights=" + _lights + ", _camera=" + _camera
				+ ", _screenDistance=" + _screenDistance + "]";
	}

	//operations
	 public void addGeometry(Geometry g)
	 {//adds a new geometry to the list
		 this._geometries.add(g);
	 }
	
	 public Iterator<Geometry> getGeometriesIterator()
		{
		 //returns an iterator to the geometries list

			return _geometries.iterator();
		}

	 public void addLight(Light l)
	 {//adds a new light to the list
		 this._lights.add(l);
	 }
	
	 public Iterator<Light> getLightsIterator()
		{
		 //returns an iterator to the lights list
			return _lights.iterator();
		}

}
