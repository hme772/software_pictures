package Primitives;

public class Ray {
	Point3D _POO;
	Vector _direction;
	
	//constructor
	public Ray(Point3D _POO, Vector _direction) {
		//super();
		this._POO = new Point3D(_POO);
		this._direction = new Vector(_direction);
	}
	public Ray(Ray r) {
		//super();
		this._POO = new Point3D(r.get_POO());
		this._direction = new Vector(r.get_direction());
	}
	
	//gets and sets values
	public Point3D get_POO() {
		return new Point3D(_POO.getX(),_POO.getY(),_POO.getZ());
	}

	public void set_POO(Point3D _POO) {
		this._POO = new Point3D(_POO.getX(),_POO.getY(),_POO.getZ());
	}

	public Vector get_direction() {
		return new Vector(new Point3D(_direction.getHead().getX(),_direction.getHead().getY(),_direction.getHead().getZ()));
	}

	public void set_direction(Vector _direction) {
		this._direction = new Vector(new Point3D(_direction.getHead().getX(),_direction.getHead().getY(),_direction.getHead().getZ()));
	}
	
	//administration
	
	@Override
	public boolean equals(Object obj)
	{
		//checks if 2 rays are the same one
		
		Ray o=(Ray)obj;
		return(o.get_direction().equals(this.get_direction()) && o.get_POO().equals(this.get_POO()));

	}

	@Override
	public String toString() {
		String str="";
		str+="Ray:"+'\n'+this._POO.toString()+'\n'+this._direction.toString();
		return str;
	}

}

