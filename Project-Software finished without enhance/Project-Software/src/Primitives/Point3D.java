package Primitives;
import java.lang.Math; 
public class Point3D extends Point2D
{
	Coordinate z;
    //constructor
	public Point3D(Coordinate x, Coordinate y, Coordinate z) {
		super(x.getX(), y.getX());
		this.z = new Coordinate(z.getX());
	}
    
	// constructor with parameters

	public Point3D(double x, double y, double z)
	{
		super(new Coordinate(x),new Coordinate(y));
		this.setZ(new Coordinate(z));
	}
	//copy constructor
	public Point3D(Point3D p)
	{
		super(p.getX().getX(),p.getY().getX());
		z = new Coordinate(p.getZ().getX());
	}
	
	//gets and sets values
	public Coordinate getZ() {
		return new Coordinate(z.getX());
	}

	public void setZ(Coordinate z) {
		this.z = new Coordinate(z.getX());
	}

	//administration
	@Override
	public boolean equals(Object obj)
	{
		if ( obj instanceof Point3D)
		{
			return((((Point3D)obj).x.getX()==this.x.getX()) && (((Point3D)obj).y.getX()==this.y.getX()) && (((Point3D)obj).z.getX()==this.z.getX()));	 
		}
		return false;
	}
	@Override
	public String toString() {
	return "Point (" + x + ","+y+","+z+")";
	}
	//operations
	public double distance(Point3D p)
	{//calculates the distance
		//to convert a coordinate to double we used getX
		double d=Math.pow(this.getX().getX()-p.getX().getX(),2.0)+Math.pow(this.getY().getX()-p.getY().getX(),2.0)+Math.pow(this.getZ().getX()-p.getZ().getX(),2.0);
		return(Math.sqrt(d));
	}
	public void add(Vector vector)
	{//adds a vector to a point
		
		this.x.add(vector.getHead().getX().getX());
		this.y.add(vector.getHead().getY().getX());
		this.z.add(vector.getHead().getZ().getX());

	}
	public void subtract(Vector vector)
	{//adds a vector to a point
		
		this.x.subtract(vector.getHead().getX().getX());
		this.y.subtract(vector.getHead().getY().getX());
		this.z.subtract(vector.getHead().getZ().getX());

	}

}
