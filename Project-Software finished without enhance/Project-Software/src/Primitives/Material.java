package Primitives;

//import java.util.Objects;

public class Material 
{


	private double _Kd; /* Diffusion attenuation coefficient � ����� ���� */
	private double _Ks; /* Specular attenuation coefficient � */  
	private double _n;  /* shininess index*/
	private double _Kr;/* reflection */
	private double _Kt;/* refraction */
	
	//empty constructor
	public Material()
	{
		super();
		this._Kd = 1;
		this._Ks = 1;
		this._n =19;
		this._Kr=this._Kt=0;
	}
	
	// constructor with parameters
	public Material(double _Kd, double _Ks, double _n, double _Kr, double _Kt)
	{
		super();
		this._Kd = _Kd;
		this._Ks = _Ks;
		this._n = _n;
		this._Kr = _Kr;
		this._Kt = _Kt;
	}

	
	// constructor with few parameters
	public Material(double _Kd, double _Ks, double _n)
	{
		super();
		this._Kd = _Kd;
		this._Ks = _Ks;
		this._n = _n;
		this._Kr=this._Kt=0;
	}
	
	
	//copy constructor

	public Material(Material m)
	{
		this._Kd=m._Kd;
		this._Ks=m._Ks;
		this._n=m._n;
		this._Kr=m._Kr;
		this._Kt=m._Kt;
	}
	


	//gets and sets values
	public double get_Kd() {
		return _Kd;
	}
	public void set_Kd(double _Kd) {
		this._Kd = _Kd;
	}
	public double get_Ks() {
		return _Ks;
	}
	public void set_Ks(double _Ks) {
		this._Ks = _Ks;
	}
	public double get_n() {
		return _n;
	}
	public void set_n(double _n) {
		this._n = _n;
	}
	
	public double get_Kr() {
		return _Kr;
	}

	public void set_Kr(double _Kr) {
		this._Kr = _Kr;
	}

	public double get_Kt() {
		return _Kt;
	}

	public void set_Kt(double _Kt) {
		this._Kt = _Kt;
	}

	//administration	
	@Override
	public boolean equals(Object obj) 
	{
		
		Material m=(Material)obj;
		return (this._Kd==m._Kd && this._Ks==m._Ks && this._n==m._n && this._Kr==m._Kr && this._Kt==m._Kt);
	}

	@Override
	public String toString() {
		return "Material [_Kd=" + _Kd + ", _Ks=" + _Ks + ", _n=" + _n + ", _Kr=" + _Kr + ", _Kt=" + _Kt + "]";
	}

	//operations

	
	//none at the moment
	
}
