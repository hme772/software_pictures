package Primitives;

public class Point2D {
	Coordinate x;
	Coordinate y;
	
	//constructor
	public Point2D(Coordinate x, Coordinate y) {
		//super();
		this.x = x;
		this.y = y;
	}
	public Point2D(double x, double y)
	{
		this.setX(new Coordinate(x));
		this.setY(new Coordinate(y));
	}
	//copy constructor
	public Point2D(Point2D p)
	{
		this.setX(p.getX());
		this.setY(p.getY());
	}
	//gets and sets values
	public Coordinate getX() {
		return new Coordinate(x.getX());
	}

	public void setX(Coordinate x) {
		this.x = new Coordinate(x.getX());
	}

	public Coordinate getY() {
		return new Coordinate(y.getX());
	}

	public void setY(Coordinate y) {
		this.y =new Coordinate(y.getX());
	}
	//administration
	@Override
	public boolean equals(Object obj)
	{
		if ( obj instanceof Point2D)
		{
			return(((Point2D)obj).x.equals(this.x )&& ((Point2D)obj).y.equals(this.y));	 
		}
		return false;
	}
	@Override
	public String toString() {
	return "Point (" + x + ","+y+")";
}

	
}
