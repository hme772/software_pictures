package Primitives;
import java.lang.Math; 

public class Vector {
	Point3D head;

	//constructor
	public Vector(Point3D head) {
		super();
		this.head = new Point3D(head);
	}
	// constructor with parameters

	public Vector(double x, double y, double z)
	{
		super();
		Point3D p=new Point3D(x,y,z);
		this.setHead(p);
	}
	//copy constructor
	public Vector(Vector v) {
		super();
		this.head = new Point3D(v.getHead());
	}
	
	//gets and sets values

	public Point3D getHead() {
		return head;
				//new Point3D(head.getX(),head.getY(),head.getZ());
	}

	public void setHead(Point3D head) {
		this.head = new Point3D(head.getX(),head.getY(),head.getZ());
	}
	
	//administration
		@Override
		public boolean equals(Object obj)
		{
			Vector o=(Vector)obj;
			if(o.getHead().equals(this.getHead()))
				return true;
			return false;
		}

		@Override
		public String toString() {
			return "Vector (" + this.getHead().getX() + ","+this.getHead().getY() +","+this.getHead().getZ() +")";
		}
	
		//operations
		public void add (Vector vector )
		{//adds a vector 
			this.head.add(vector);
		}
		public void subtract (Vector vector ) 
		{//subtracts a vector 
			this.head.subtract(vector);
		}
		public void scale(double scalingFacor) 
		{//scalar multiplication
			//we have multiplied the values of each coordinate by scalingFacor-1 
			double _x=(this.getHead().getX().getX())*(scalingFacor-1);
			double _y=(this.getHead().getY().getX())*(scalingFacor-1);
			double _z=(this.getHead().getZ().getX())*(scalingFacor-1);
			//we have created a vector which holds the values of this vector multiplied by scalingFacor-1
			Vector v=new Vector(new Point3D(new Coordinate(_x),new Coordinate(_y),new Coordinate(_z)));
			//by adding v to this vector we'll achieve the vector * scalingFacor
			this.add(v);
		}
		public double length() 
		{//the distance from it's start (which is (0,0,0)
			return this.getHead().distance(new Point3D(new Coordinate(0.0),new Coordinate(0.0),new Coordinate(0.0)));
		}
		public void normalize() 
		{//normalizes a vector meaning that we will divide it by it's length
			double dSquare=Math.pow(this.getHead().getX().getX(),2)+Math.pow(this.getHead().getY().getX(),2)+Math.pow(this.getHead().getZ().getX(),2);
			double d=Math.sqrt(dSquare);
			double newX=(this.getHead().getX().getX())/d;
			double newY=(this.getHead().getY().getX())/d;
			double newZ=(this.getHead().getZ().getX())/d;
			this.getHead().setX(new Coordinate(newX));
			this.getHead().setY(new Coordinate(newY));
			this.getHead().setZ(new Coordinate(newZ));
		}
		
		private double [] doubleVector(Vector vector)
		{//for more convenient usage we created a function to return the vector's values as double vector
			double u1=vector.getHead().getX().getX();
			double u2=vector.getHead().getY().getX();
			double u3=vector.getHead().getZ().getX();
			double [] array= {u1,u2,u3};
			return array;
			
		}
		public Vector crossProduct (Vector vector)
		{//creates a vector which is the cross product of this and another vector
			double[] U= doubleVector(this);
			double[] V=doubleVector(vector);
	
			
			double result1=U[1]*V[2] -U[2]*V[1];
			double result2=U[2]*V[0] -U[0]*V[2];
			double result3=U[0]*V[1] -U[1]*V[0];
		
			Vector resultVector=new Vector(new Point3D(new Coordinate(result1),new Coordinate(result2),new Coordinate(result3)));
			return resultVector;
		}
		public double dotProduct(Vector vector)
		{
			//calculates the dot product of 2 vectors
			
			double[] U= doubleVector(this);
			double[] V=doubleVector(vector);
			double sum= 0;
			for(int i=0;i<3;i++)
			{
				sum+=U[i]*V[i];
			}
			return sum;
		}
}
