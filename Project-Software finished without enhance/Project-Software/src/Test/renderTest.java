package Test;

//import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

//import javax.imageio.ImageWriter;

import org.junit.jupiter.api.Test;

//import Elements.AmbientLight;
import Elements.PointLight;
import Elements.SpotLight;
import Geometries.Rhombus;
import Geometries.Sphere;
import Geometries.Triangle;
import Primitives.Material;
import Primitives.Point3D;
import Primitives.Vector;
//import Primitives.Vector;
import Renderer.Render;
import Renderer.ImageWriter;
import Scene.Scene;

class renderTest {


	@Test
	
		public void basicRendering(){
			
			Scene scene = new Scene();
			
			scene.addGeometry(new Sphere(Color.blue,50.0, new Point3D(0.0, 0.0, -150)));
			
			Triangle triangle = new Triangle(Color.PINK,new Point3D( 100, 0, -149),
					 						 new Point3D(  0, 100, -149),
					 						 new Point3D( 100, 100, -149));
			
			Triangle triangle2 = new Triangle(Color.yellow,new Point3D( 100, 0, -149),
					 			 			  new Point3D(  0, -100, -149),
					 			 			  new Point3D( 100,-100, -149));
			
			Triangle triangle3 = new Triangle(Color.yellow,new Point3D(-100, 0, -149),
					 						  new Point3D(  0, 100, -149),
					 						  new Point3D(-100, 100, -149));
			
			Triangle triangle4 = new Triangle(Color.pink,new Point3D(-100, 0, -149),
					 			 			  new Point3D(  0,  -100, -149),
					 			 			  new Point3D(-100, -100, -149));
			
			scene.addGeometry(triangle);
			scene.addGeometry(triangle2);
			scene.addGeometry(triangle3);
			scene.addGeometry(triangle4);
			
			ImageWriter imageWriter = new ImageWriter("Render test", 500, 500, 500, 500);
			
			Render render = new Render(scene, imageWriter);
			
			render.renderImage();
			render.printGrid(50);
			imageWriter.writeToimage();
		}





	@Test
	public void pointLightTest1(){
		
		Scene scene = new Scene();
		//scene.set_ambientLight(new AmbientLight(new Color(0,0,0),1.0));
		scene.set_screenDistance(100);
		Sphere sphere = new Sphere  (new Color(0,0,100),800, new Point3D(0,0,-1000));
		Material m=new Material();
		m.set_n(20);
		sphere.set_material(m);
		scene.addGeometry(sphere);
		scene.addLight(new PointLight(new Color(255,100,100), new Point3D(-200, -200,-100),//-200, -200, -100), 
				   1, 0.00001, 0.000005));
	
		ImageWriter imageWriter = new ImageWriter("Point Test1", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		//render.printGrid(50);
		imageWriter.writeToimage();
		
	}
	

	@Test
	public void pointLightTest2(){
		Scene scene = new Scene();
		scene.set_screenDistance(100);
		Sphere sphere = new Sphere (new Color(0,0,100),400, new Point3D(0,0, -1000));
		Material m=new Material();
		m.set_n(20);
		sphere.set_material(m);
		
	
		Triangle triangle = new Triangle(new Color(0,0,0), new Point3D(  3500, 3500, -2000),
					  new Point3D( -3500, -3500, -1000),  new Point3D(3500, -3500, -2000) );
		Triangle triangle2 = new Triangle(new Color(0,0,0), new Point3D(   3500, 3500, -2000),
						new Point3D(   -3500, 3500, -1000), new Point3D( -3500, -3500, -1000));
		scene.addGeometry(sphere);
		scene.addGeometry(triangle);
		scene.addGeometry(triangle2);
		
		scene.addLight(new PointLight(new Color(255,100,100), new Point3D(-200,200, -100),
	1, 0.000001, 0.0000005));

		
		ImageWriter imageWriter = new ImageWriter("Point Test2", 500, 500, 500, 500);
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		//render.printGrid(50);
		imageWriter.writeToimage();
	}
	
	

	@Test
	public void spotLightTest1(){
		
		Scene scene = new Scene();
		scene.set_screenDistance(100);
		Sphere sphere = new Sphere (new Color(0,0,100),800, new Point3D(0,0, -1000));
		
		
		Material m=new Material();
		m.set_n(20);
		sphere.set_material(m);
		scene.addGeometry(sphere);
				
		scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(-200, -200, -100), 
					  1, 0.00001, 0.000005,   new Vector(2, 2, -3)));
	
		ImageWriter imageWriter = new ImageWriter("Spot Test1", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		//render.printGrid(50);
		imageWriter.writeToimage();
	}
	
	@Test
	public void spotLightTest2(){
		
		Scene scene = new Scene();
		scene.set_screenDistance(200);
		
		Sphere sphere = new Sphere (new Color(0,0,100),500, new Point3D(0,0,-1000));
		
		Material m=new Material();
		m.set_n(20);
		m.set_Kd(1);
		sphere.set_material(m);
		scene.addGeometry(sphere);
		
		Triangle triangle = new Triangle(new Color (0, 0, 100),
										 new Point3D(-125, -225, -260),
										 new Point3D(-225, -125, -260),
										 new Point3D(-225, -225, -270)
									);
		
	//	Sphere sphere2 = new Sphere (new Color(0,0,100),50, new Point3D(-175,-175,-260));

		Material m1=new Material();
		m1.set_n(4);
		m1.set_Kd(1);
		m1.set_Ks(1);
		triangle.set_material(m1);
		scene.addGeometry(triangle);
	
		scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(-200, -200, -150), 
					  1, 0.00001, 0.000005,   new Vector(2, 2, -3)));
	
		ImageWriter imageWriter = new ImageWriter("Spot Test2", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		//render.printGrid(50);
		imageWriter.writeToimage();
	}

	@Test
	public void spotLightTest3(){
		
		
		Scene scene = new Scene();
		scene.set_screenDistance(100);
		
		Triangle triangle = new Triangle(new Color(0,0,0),
										 new Point3D(  3500,  3500, -2000),
				 						 new Point3D( -3500, -3500, -1000),
				 						 new Point3D(  3500, -3500, -2000)
				 						 );

		
		
		Triangle triangle2 = new Triangle(new Color(0,0,0),
										  new Point3D(  3500,  3500, -2000),
				  						  new Point3D( -3500,  3500, -1000),
				  						  new Point3D( -3500, -3500, -1000)
					 						);
	
		scene.addGeometry(triangle);
		scene.addGeometry(triangle2);

	scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(200, 200, -100), 
					    1, 0.000001, 0.0000005, new Vector(-2, -2, -3)));
	
		
		ImageWriter imageWriter = new ImageWriter("Spot Test3", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
	//	render.printGrid(50);
		imageWriter.writeToimage();
		
	}
	
	
	@Test
	public void somespotLightTest(){
		
		Scene scene = new Scene();
		scene.set_screenDistance(200);
		
		Sphere sphere = new Sphere (new Color(0,0,100),500, new Point3D(0,0,-1000));
		
		Material m=new Material();
		m.set_n(20);
		m.set_Kd(1);
		sphere.set_material(m);
		scene.addGeometry(sphere);
		

		
		Sphere sphere2 = new Sphere (new Color(0,0,100),50, new Point3D(-175,-175,-260));

		Material m1=new Material();
		m1.set_n(4);
		m1.set_Kd(1);
		m1.set_Ks(1);
		sphere2.set_material(m1);
		scene.addGeometry(sphere2);
	
		scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(-200, -200, -150), 
					  1, 0.00001, 0.000005,   new Vector(2, 2, -3)));
	
		ImageWriter imageWriter = new ImageWriter("Some Spot Test", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		//render.printGrid(50);
		imageWriter.writeToimage();
	}

	
	@Test
	public void shadowTest(){
		
		
		Scene scene = new Scene();
		scene.set_screenDistance(100);
		
		Sphere sphere = new Sphere (new Color(0,0,100),400, new Point3D(0,0, -1000));
		Material m=new Material();
		m.set_n(20);
		sphere.set_material(m);
		
		Triangle triangle = new Triangle(new Color(0,0,0),
										 new Point3D(  3500,  3500, -2000),
				 						 new Point3D( -3500, -3500, -1000),
				 						 new Point3D(  3500, -3500, -2000)
				 						 );

		
		
		Triangle triangle2 = new Triangle(new Color(0,0,0),
										  new Point3D(  3500,  3500, -2000),
				  						  new Point3D( -3500,  3500, -1000),
				  						  new Point3D( -3500, -3500, -1000)
					 						);
	
		scene.addGeometry(triangle);
		scene.addGeometry(triangle2);
		scene.addGeometry(sphere);

	scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(200, 200, -100), 
					    1, 0.000001, 0.0000005, new Vector(-2, -2, -3)));
	
		
		ImageWriter imageWriter = new ImageWriter("Shadow", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
	//	render.printGrid(50);
		imageWriter.writeToimage();
		
	}
	
	

	@Test
	public void test4()
	{
		Scene scene=new Scene();
		ImageWriter iw=new ImageWriter("Rhombus",500,500,500,500);
		scene.set_sceneName("t");
		scene.set_screenDistance(100);

		scene.addGeometry(new Rhombus(Color.pink,new Vector(0,-1000,0),new Vector(-500,500,0),new Point3D(2000,2000,-2000)));
		scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(200, 200, -100), 
			    1, 0.000001, 0.0000005, new Vector(-2, -2, -3)));

		Render render = new Render(scene, iw);
		
		render.renderImage();
		iw.writeToimage();
	}

	
}
