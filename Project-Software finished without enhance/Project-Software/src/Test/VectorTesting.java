package Test;
import Primitives.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class VectorTesting {

@Test
	void testAdd()
	{
		Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(3)));
		Vector v2=new Vector(new Point3D(new Coordinate(2),new Coordinate(4),new Coordinate(6)));
		v1.add(v2);
		Vector v3=new Vector(new Point3D(new Coordinate(3),new Coordinate(6),new Coordinate(9)));
		assertEquals(v1,v3);
	}
@Test
	void testSubtract()
	{
	Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(3)));
	Vector v2=new Vector(new Point3D(new Coordinate(2),new Coordinate(4),new Coordinate(6)));
	v1.subtract(v2);
	Vector v3=new Vector(new Point3D(new Coordinate(-1),new Coordinate(-2),new Coordinate(-3)));
	assertEquals(v1,v3);
	}
@Test
	void testScaling() 
	{
	Vector v2=new Vector(new Point3D(new Coordinate(2),new Coordinate(4),new Coordinate(6)));
	v2.scale(0.5);
	Vector v3=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(3)));
	assertEquals(v2,v3);
	}
@Test
	void testDotProduct() 
	{
	Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(3)));
	Vector v2=new Vector(new Point3D(new Coordinate(2),new Coordinate(4),new Coordinate(6)));
	double d=v1.dotProduct(v2);
	assertEquals(d,28.0);
	}
@Test
	void testCrossProduct() 
	{
	Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(1),new Coordinate(1)));
	Vector v2=new Vector(new Point3D(new Coordinate(1),new Coordinate(1),new Coordinate(1)));
	Vector v3=v1.crossProduct(v2);
	Vector v4=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)));
	assertEquals(v4,v3);
	}
@Test
	void testLength() 
	{
	Vector v2=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(2)));
	assertEquals(3,v2.length());
	}
@Test
	void testNormalize() 
	{
	Vector v2=new Vector(new Point3D(new Coordinate(3),new Coordinate(0),new Coordinate(0)));
	v2.normalize();
	Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
	assertEquals(v1,v2);
	}

}


