package Test;
//import Primitives.*;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import Geometries.Plane;
import Geometries.Triangle;
import Primitives.Coordinate;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;


//import org.junit.jupiter.api.Test;

class TriangleTesting {

	@Test
	void testGetNormal() 
	{
		Triangle t=new Triangle(Color.BLACK,new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)),new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Vector n=t.getNormal(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)));
		Vector temp=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(1)));

		assertEquals(n,temp);
		
	}

	@Test
	public void testTriangleIntersections() {

		// creating the expected values
		
		List<Point3D> answerList = new ArrayList<Point3D>();		
		Point3D answerPoint = new Point3D(0, 0, -200);		
		answerList.add(answerPoint);
		
		// building the triangle
		
		Point3D p1 = new Point3D(0, 100, -200);
		Point3D p2 = new Point3D(100, -100, -200);
		Point3D p3 = new Point3D(-100, -100, -200);
		
		Triangle t1 = new Triangle(Color.white, p1, p2, p3);
		Triangle t2 = new Triangle(t1);			
		
		// building the ray that will intersect the triangle
		
		Point3D centerPoint = new Point3D(0,0,0);		
		Vector vector = new Vector(0, 0, -5);
		Ray ray = new Ray(centerPoint, vector);
	
		// testing the findIntersection function
		
		List<Point3D> list = new ArrayList<Point3D>();
		list = t2.findIntersections(ray);
		assertEquals(answerList, list);			
	}
	
	
	

}
