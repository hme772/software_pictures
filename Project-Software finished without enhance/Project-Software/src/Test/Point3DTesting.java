package Test;
import Primitives.*;
//import Primitives.Point3D;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Point3DTesting {

	@Test
	void testAdd() {
		Point3D p1=new Point3D(new Coordinate(2.0),new Coordinate(4.0),new Coordinate(6.0));
		Vector v=new Vector(new Point3D(new Coordinate(1.0),new Coordinate(-1.0),new Coordinate(-3.0)));
		p1.add(v);
		Point3D p4=new Point3D(new Coordinate(3.0),new Coordinate(3.0),new Coordinate(3.0));
		assertEquals(p4,p1);
	}
	@Test
	void testSubtract() {
		Point3D p1=new Point3D(new Coordinate(2.0),new Coordinate(4.0),new Coordinate(6.0));
		Vector v=new Vector(new Point3D(new Coordinate(1.0),new Coordinate(-1.0),new Coordinate(-3.0)));
		p1.subtract(v);
		Point3D p4=new Point3D(new Coordinate(1.0),new Coordinate(5.0),new Coordinate(9.0));
		assertEquals(p4,p1);
	}
}
