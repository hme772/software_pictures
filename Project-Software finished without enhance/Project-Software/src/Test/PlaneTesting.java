package Test;
//import Primitives.*;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import Geometries.Plane;
import Primitives.Coordinate;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;

class PlaneTesting {

	@Test
	void testGetNormal() 
	{
	Plane p1=new Plane(new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0))), new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),  Color.BLACK);	
	Vector v=p1.getNormal(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(18)));
	assertEquals(v,new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0))));
	
	}

	@Test
	void findIntersections()
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		Plane p1=new Plane(new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(1))), new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),  Color.BLACK);	

		Vector P0 = new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)));//P0
		Vector v=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));//direction
		P0.subtract(new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0))));//P0-Q0
		double NV= p1.getNormal().dotProduct(v);//N*V
		if(NV!=0)
		{//if it's equal to 0 it will cause a running time error
			double t=-((p1.getNormal().dotProduct(P0))/NV);//-N*(P0-Q0)/N*V
			if (t>0)
			{//calculates the point of the intersection
				v.scale(t);
				Point3D p=new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
				p.add(v);
				lst.add(p);//adds the point to the list
			}
		}	
		Ray r=new Ray(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1))));
		assertEquals(lst,p1.findIntersections(r));
	}
	@Test
	void findIntersections1()
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		Plane p1=new Plane(new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(1))), new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),  Color.BLACK);	

		Vector P0 = new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)));//P0
		Vector v=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(-3)));//direction
		P0.subtract(new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0))));//P0-Q0
		double NV= p1.getNormal().dotProduct(v);//N*V
		if(NV!=0)
		{//if it's equal to 0 it will cause a running time error
			double t=-((p1.getNormal().dotProduct(P0))/NV);//-N*(P0-Q0)/N*V
			if (t>0)
			{//calculates the point of the intersection
				v.scale(t);
				Point3D p=new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
				p.add(v);
				lst.add(p);//adds the point to the list
			}
		}	
		Ray r=new Ray(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(-3))));
		assertEquals(lst,p1.findIntersections(r));
	}
	

}
