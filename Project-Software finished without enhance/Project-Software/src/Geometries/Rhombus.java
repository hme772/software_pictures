package Geometries;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
//import java.util.Objects;

import Primitives.Material;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;

public class Rhombus extends Geometry implements FlatGeometry
{
	//������,���� ��� ����� �� �����,����,������� ���� 
 //Rhombus is made of 2 Vectors and a point
	Vector v1;
	Vector v2;
	Point3D p;

	//constructor

	public Rhombus(Color emission, Material m, Vector v1, Vector v2, Point3D p) {
		super(emission, m);
		this.v1 = v1;
		this.v2 = v2;
		this.p = p;
	}
	 
	public Rhombus(Color emission, Vector v1, Vector v2, Point3D p) {
		super(emission);
		this.v1 = v1;
		this.v2 = v2;
		this.p = p;
	}
	
	//copy constructor

	public Rhombus(Rhombus r)
	{
		super(r.emission, r._material);
		this.v1 = r.v1;
		this.v2 = r.v2;
		this.p = r.p;
	}
	
	//gets and sets values

	public Vector getV1() {
		return v1;
	}

	public void setV1(Vector v1) {
		this.v1 = v1;
	}

	public Vector getV2() {
		return v2;
	}

	public void setV2(Vector v2) {
		this.v2 = v2;
	}

	public Point3D getP() {
		return p;
	}

	public void setP(Point3D p) {
		this.p = p;
	}
	
	



	//administration

	@Override
	public String toString() {
		return "Rhombus [v1=" + v1 + ", v2=" + v2 + ", p=" + p + "]";
	}
	


	@Override
	public boolean equals(Object obj) 
	{
		
		Rhombus o= (Rhombus)obj;
		return (o._material.equals(this._material) && this.getP().equals(o.getP()) && this.getV1().equals(o.getV1()) && this.getV2().equals(o.getV2()) && this.getEmission()==o.getEmission());

	}
	
	//operations


	@Override
	public Vector getNormal(Point3D p) {
		//calculates the normal
		Vector n=(v1.crossProduct(v2));//instead of creating a new vector as the 
		n.normalize();
		//n.scale(-1);
		return n;
	}
	@Override
	public List<Point3D> findIntersections(Ray r) 
	{
		//calculates the intersection points and returns it as a list
		//creates 4 points of the rhombus
		Point3D a=new Point3D(this.p);
		Point3D b=new Point3D(this.getP());
		b.add(getV1());
		Point3D c=new Point3D(this.getP());
		c.add(getV2());
		Point3D d=new Point3D(b);
		d.add(getV2());
		
		//creates 2 triangles (abc, bcd)
		Triangle t1=new Triangle(this.getEmission(),a,b,c);
		t1.set_material(this.get_material());
		Triangle t2=new Triangle(this.getEmission(),b,c,d);
		t2.set_material(this.get_material());

		List<Point3D> lst=new ArrayList<Point3D>();//the final list to be returned
		
		//calculates the intersections of the 2 triangles and joins them to the list
		List<Point3D> lst1=new ArrayList<Point3D>();
		lst1=(t1.findIntersections( r)) ;
		List<Point3D> lst2=new ArrayList<Point3D>();
		lst2=t2.findIntersections( r) ;
		lst.addAll(lst1);
		lst.addAll(lst2);


		return lst;
	}

}
