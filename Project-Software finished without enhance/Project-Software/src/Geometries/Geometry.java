package Geometries;
import java.awt.Color;
import java.util.List;

import Primitives.*;
//import primitives.Material;
public abstract class Geometry 
{
	//every goemetry will inherit from this class, also every geometry has a material which it's made of and an emission
	Color emission;
	Material _material;

	//constructor
	public Geometry(Color emission, Material m) {
		super();
		this.emission = emission;
		this._material=m;
	}
	//copy constructor
	public Geometry(Geometry g)
	{
		super();
		this.emission=g.emission;
		this._material=g._material;
	}
	//constructor that have only emission
	public Geometry(Color _emission) {
		super();
		this.emission = _emission;
		_material = new Material();
	}
	

	//gets and sets values
	public Color getEmission()
	{
		return emission;
	}

	public void setEmission(Color emission)
	{
		this.emission = emission;
	}
	
	public Material get_material() {
		return _material;
	}

	public void set_material(Material _material) {
		this._material = _material;
	}
	//administration
	@Override
	public String toString() {
		return "Geometry [emission=" + emission + ", _material=" + _material + "]";
	}
	
	//operations
	abstract public Vector getNormal(Point3D p);

	abstract public List<Point3D> findIntersections(Ray r);

	
}
