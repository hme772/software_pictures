package Geometries;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import Primitives.*; 

public class Plane extends Geometry implements FlatGeometry
{
	//a plane is made of a point and a normal 
	Point3D p;
	Vector normal;
	
	//constructor
	public Plane( Vector normal,Point3D p, Color e) {
		super(e);
		this.p = p;
		this.normal = normal;
	}
	//gets and sets values
	public Point3D getP() {
		return new Point3D(p.getX(),p.getY(),p.getZ());
	}

	public void setP(Point3D p) {
		this.p = new Point3D(p.getX(),p.getY(),p.getZ());
	}
	public Vector getNormal() {
		return new Vector(new Point3D(normal.getHead().getX(),normal.getHead().getY(),normal.getHead().getZ()));
	}
	public void setNormal(Vector normal)
	{
		this.normal =  new Vector(new Point3D(normal.getHead().getX(),normal.getHead().getY(),normal.getHead().getZ()));
	}
	
	//administration
	@Override
	public String toString() {
		return super.toString()+"Plane [p=" + p + ", normal=" + normal + "]";
	}
	
	@Override
	public boolean equals(Object obj) 
	{

		Plane o = (Plane) obj;
		return (o._material.equals(this._material) && o.getEmission()==this.getEmission() && o.getNormal().equals(this.getNormal()) && o.getP().equals(this.getP()));
	}
	
	
	//operations
	@Override
	public Vector getNormal(Point3D p)
	{
		//returns the normal to the plane
		return new Vector(new Point3D(this.getNormal().getHead().getX(),this.getNormal().getHead().getY(),this.getNormal().getHead().getZ()));
	}
	
	public List<Point3D> findIntersections(Ray r)
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		Vector P0 = new Vector(r.get_POO());//P0
		Vector v=r.get_direction();//direction
		P0.subtract(new Vector(this.getP()));//P0-Q0
		double NV= this.getNormal().dotProduct(v);//N*V
		if(NV!=0)
		{//if it's equal to 0 it will cause a running time error
			double t=-((this.getNormal().dotProduct(P0))/NV);//-N*(P0-Q0)/N*V
			if (t>0)
			{//calculates the point of the intersection
				v.scale(t);
				Point3D p=new Point3D(r.get_POO().getX(),r.get_POO().getY(),r.get_POO().getZ());
				p.add(v);
				lst.add(p);//adds the point to the list
			}
		}	
		return lst;
	}

	


}
