package Geometries;
import java.awt.Color;
//import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import Primitives.*; 

public class Triangle extends Geometry implements FlatGeometry
{
	//a triangle is made of 3 points
	
	Point3D p1;
	Point3D p2;
	Point3D p3;


	//constructor
	
	public Triangle(Color emission, Point3D p1, Point3D p2, Point3D p3 ) {
		super(emission);
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	//copy constructor
	public Triangle(Triangle t)
	{
		super(t.emission, t._material);
		this.p1 = t.getP1();
		this.p2 = t.getP2();
		this.p3 = t.getP3();
		
	}
	//gets and sets values
	
	public Point3D getP1()
	{
		return new Point3D(p1.getX(),p1.getY(),p1.getZ());
	}

	public void setP1(Point3D p1) {
		this.p1 = new Point3D(p1.getX(),p1.getY(),p1.getZ());
	}

	public Point3D getP2() {
		return  new Point3D(p2.getX(),p2.getY(),p2.getZ());
	}

	public void setP2(Point3D p2) {
		this.p2 =  new Point3D(p2.getX(),p2.getY(),p2.getZ());
	}

	public Point3D getP3() {
		return  new Point3D(p3.getX(),p3.getY(),p3.getZ());
	}

	public void setP3(Point3D p3) {
		this.p3 =  new Point3D(p3.getX(),p3.getY(),p3.getZ());
	}

	
	//administration
	@Override
	public String toString() {
		return "Triangle [p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + "]";
	}

	@Override
	public boolean equals(Object obj) {
		Triangle o= (Triangle)obj;
		return (o._material.equals(this._material) && this.getP1().equals(o.getP1()) && this.getP2().equals(o.getP2()) && this.getP3().equals(o.getP3()) && this.getEmission()==o.getEmission());

	}


	//operations
	@Override
	public Vector getNormal(Point3D p) 
	{//calculates the normal
		Point3D t1=new Point3D(this.getP2().getX(),this.getP2().getY(),this.getP2().getZ());//a copy of p2
		Vector temp=new Vector(new Point3D(this.getP1().getX(),this.getP1().getY(),this.getP1().getZ()));//a vector that has the values of p1
		t1.subtract(temp);//p2-p1=v1
		Vector v1=new Vector(new Point3D(t1.getX(),t1.getY(),t1.getZ()));
		Point3D t2=new Point3D(this.getP3().getX(),this.getP3().getY(),this.getP3().getZ());//a copy of p3
		t2.subtract(temp);//p3-p1=v2
		Vector v2=new Vector(new Point3D(t2.getX(),t2.getY(),t2.getZ()));
		Vector n=(v1.crossProduct(v2));//instead of creating a new vector as the 
		n.normalize();
		//n.scale(-1);
		return n;
	}

	public List<Point3D> findIntersections(Ray r)
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		//create v1 v2
		Point3D p1 = new Point3D(this.getP1());
		p1.subtract(new Vector(this.getP2()));
		Vector v1=new Vector(p1);//v1=p1-p2
		Point3D p2 = new Point3D(this.getP1());
		p2.subtract(new Vector(this.getP3()));
		Vector v2=new Vector(p2);//v2=p2-p3
		//create plane
		Vector normal = v1.crossProduct(v2);
		normal.normalize();
		Plane plane = new Plane(normal, this.getP1(), Color.BLACK);
		//checks intersection points with the plane
		lst= plane.findIntersections(r);
		
		//if there are no intersections 
		if(lst.isEmpty()==true)
			return lst;
		
		//creates V1 V2 V3
		Point3D T1=new Point3D(this.getP1());
		Point3D T2=new Point3D(this.getP2());
		Point3D T3=new Point3D(this.getP3());
		Vector P0=new Vector(r.get_POO());
		T1.subtract(P0);
		Vector V1 = new Vector(T1);//V1=T1-P0
		T2.subtract(P0);
		Vector V2 = new Vector(T2);//V2=T2-P0
		T3.subtract(P0);
		Vector V3 = new Vector(T3);//V3=T3-P0
		
		//create N1 N2 N3
		Vector N1 = new Vector(V1.crossProduct(V2));
		N1.normalize();
		Vector N2 = new Vector(V3.crossProduct(V1));
		N2.normalize();
		Vector N3 = new Vector(V2.crossProduct(V3));
		N3.normalize();


		//create p-p0
		Point3D p=new Point3D(lst.get(0));
		p.subtract(P0);
		
		//sign((P-P0)・N1) ==sign((P-P0)・N2) == sign((P-P0)・N3) is positive
		if(N1.dotProduct(new Vector(p))> 0 && N2.dotProduct(new Vector(p))>0 && N3.dotProduct(new Vector(p))>0)
			return lst;
		
		//sign((P-P0)・N1) ==sign((P-P0)・N2) == sign((P-P0)・N3) is negative
		if(N1.dotProduct(new Vector(p))< 0 && N2.dotProduct(new Vector(p))<0 && N3.dotProduct(new Vector(p))<0)
			return lst;
		
		
		return new ArrayList<Point3D>();
	}
	

}

