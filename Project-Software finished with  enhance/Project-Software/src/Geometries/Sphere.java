package Geometries;
import java.awt.Color;
//import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import Primitives.*; 
import java.lang.Math; 

public class Sphere extends Geometry 
{

	//a shpere is made of a radius and a center
	double _radius;
	Point3D _center;
	
	//constructor
	public Sphere(Color emission, double _radius, Point3D _center ) {
		super(emission);
		this._radius = _radius;
		this._center = _center;
	}

	
	//gets and sets values
	public double get_radius() {
		return _radius;
	}

	public void set_radius(double _radius) {
		this._radius = _radius;
	}

	public Point3D get_center() {
		return new Point3D(_center.getX(),_center.getY(),_center.getZ());
	}

	public void set_center(Point3D _center) {
		this._center = new Point3D(_center.getX(),_center.getY(),_center.getZ());
	}



	//administration
	@Override
	public String toString() {
		return super.toString()+"Sphere [_radius=" + _radius + ", _center=" + _center /*+ ", _direction=" + _direction*/ + "]";
	}

	@Override
	public boolean equals(Object obj) 
	{
		//checks if 2 speres are the same one
		Sphere o = (Sphere) obj;
		return (o._material.equals(this._material) && o.getEmission()==this.getEmission() /*&& o.get_direction().equals(this.get_direction()) */&& o.get_center().equals(this.get_center()) && o.get_radius()==this.get_radius());
	}

	//operations
	@Override
	public Vector getNormal(Point3D p) 
	{//calculates the normal
		Point3D p1 = new Point3D(p);
		Vector temp=new Vector(new Point3D(this.get_center().getX(),this.get_center().getY(),this.get_center().getZ()));//a vector that has the values of center
		p1.subtract(temp);
		Vector n=new Vector(new Point3D(p1.getX(),p1.getY(),p1.getZ()));//the normal is the vector from center to p and this vector starts at p
		n.normalize();
		return n;
	}

	public List<Point3D> findIntersections(Ray r)
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		Vector P0 = new Vector(new Point3D(r.get_POO()));//P0
		Vector O = new Vector(new Point3D(this.get_center().getX(),this.get_center().getY(),this.get_center().getZ()));//O
		O.subtract(P0);
		Vector L=O;//L=O-P0
		Vector v=new Vector (r.get_direction().getHead());//direction
		double tm=L.dotProduct(v);//tm=L*V
		double d=Math.sqrt(Math.pow(L.length(),2)-Math.pow(tm, 2));//d=(L^2-tm^2)^0.5
		double th=Math.sqrt(Math.pow(this._radius,2)-Math.pow(d,2));
		double t1=tm-th;
		double t2=tm+th;
		if(t1>0)
		{
			//calculates the point of the intersection
			v.scale(t1);
			Point3D p1=new Point3D(r.get_POO().getX(),r.get_POO().getY(),r.get_POO().getZ());
			p1.add(v);
			lst.add(p1);//adds the point to the list
			v.scale(1/t1);//before we multiply it by t1 so now we return it to it's original value
		}
		
		if(t2>0)
		{
			//calculates the point of the intersection
			v.scale(t2);
			Point3D p2=new Point3D(r.get_POO().getX(),r.get_POO().getY(),r.get_POO().getZ());
			p2.add(v);
			lst.add(p2);//adds the point to the list	
		}
		return lst;
	}
	
}
