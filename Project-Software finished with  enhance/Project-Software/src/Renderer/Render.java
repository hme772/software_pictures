package Renderer;
import java.awt.Color;
import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
//import java.util.Objects;
import java.util.Map;
import java.util.Map.Entry;

import Elements.Camera;
import Elements.Light;
import Geometries.*;
//import Geometries.Geometry;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;
import Scene.*;
public class Render 
{
	
	private Scene _scene;
	private ImageWriter _imageWriter;
	public static final double RECURSION_LEVEL=3;//used to be 5
	
	//empty constructor
	public Render() {
		super();
		this._scene = new Scene();
		this._imageWriter = new ImageWriter("empty ctor",500,500,500,500);
	}
	
	// constructor with parameters

	public Render(Scene _scene, ImageWriter _imageWriter) {
		super();
		this._scene = _scene;
		this._imageWriter = _imageWriter;
	}
	
	//copy constructor
	public Render(Render r)
	{
		this._scene=new Scene(r._scene );
		this._imageWriter=new ImageWriter(r._imageWriter );
		
	}
	
	//gets and sets values

	public Scene get_scene() {
		return _scene;
	}

	public void set_scene(Scene _scene) {
		this._scene = _scene;
	}

	public ImageWriter get_imageWriter() {
		return _imageWriter;
	}

	public void set_imageWriter(ImageWriter _imageWriter) {
		this._imageWriter = _imageWriter;
	}

	//administration
	@Override
	public boolean equals(Object obj) 
	{
		Render o = (Render) obj;
		return (o._imageWriter.equals(this._imageWriter) && o._scene.equals(this._scene));
		}

	@Override
	public String toString()
	{
		return "Render [_scene=" + _scene + ", _imageWriter=" + _imageWriter + "]";
	
	}
		
	//operations
	public void printGrid(int interval)
	{//prints a grid
		for(int i=0;i<_imageWriter.getWidth();i++)
		{
			for(int j=0;j<_imageWriter.getHeight();j++)
			{
				if(i%interval==0 || j%interval==0)
					_imageWriter.writePixel(i, j, Color.WHITE);
			}
		}
	}
		
	public void renderImage()
	{//prints the image
		Ray ray;
		Camera camera =_scene.get_camera();
		for(int i=0;i<this._imageWriter.getWidth();i++)
		{
			for(int j=0;j<this._imageWriter.getHeight();j++)
			{
				 ray = camera.constructRayThroughPixel(_imageWriter.getNx(),_imageWriter.getNy(), 
						 j, i,_scene.get_screenDistance(), _imageWriter.getWidth(), 	_imageWriter.getHeight());

				Map<Geometry,List<Point3D>> intersectionPoints = getSceneRayIntersections(ray);
				if( intersectionPoints.isEmpty())
					_imageWriter.writePixel(j, i, this._scene.get_background());
				else
				{
					Map<Geometry,Point3D> closestPoint = getClosestPoint(intersectionPoints);
					for(Entry<Geometry,Point3D> entry:closestPoint.entrySet())
					_imageWriter.writePixel(j, i, calcColor(entry.getKey(),entry.getValue(),ray));
				}
		
			}
		}
	}

	private Map <Geometry,List<Point3D>> getSceneRayIntersections(Ray ray)
	{//returns all the intersection points of ray and the scene
		Iterator<Geometry> geometries =  _scene.getGeometriesIterator();
		 Map <Geometry,List<Point3D>> intersectionPoints = new HashMap<Geometry, List<Point3D>>();

		while (geometries.hasNext())
		{
		Geometry geometry = geometries.next();	
		List<Point3D> geometryIntersectionPoints =geometry.findIntersections(ray);
		if(!(geometryIntersectionPoints).isEmpty())intersectionPoints.put(geometry,geometryIntersectionPoints);
		}
		return intersectionPoints; 	
	}
	
	private Map<Geometry,Point3D> getClosestPoint(Map<Geometry,List<Point3D>> intersectionPoints)
	{
		// a function to find the closest point to the camera
		
		
		double distance = Double.MAX_VALUE;
		Point3D P0 = this._scene.get_camera().get_P0();
		Map<Geometry,Point3D> minDistancePoint = new HashMap<Geometry,Point3D>();

		for (Entry<Geometry, List<Point3D>> entry:intersectionPoints.entrySet())
		{
			for (Point3D point: entry.getValue()) 
			{
				if (P0.distance(point) < distance /*&& this._scene.get_screenDistance() < P0.distance(point)*/)
				{
					//the distance between the camera and point must be bigger than the screen distance but smaller than distance
					minDistancePoint.clear(); 
					minDistancePoint.put(entry.getKey(), new Point3D(point));
					distance = P0.distance(point);
				}
			}
		}
		return minDistancePoint;
		}
	
	    public Color calcDiffusiveComp(double Kd,Vector N,Vector L , Color I)
	    {
	    	
			//calculates the diffuse color in a given point according the given formula

	    	N.normalize();
	    	L.normalize();
	    	double num1=Math.abs(N.dotProduct(L));
	    	if (num1 < 0)
	    		return new Color(0,0,0);

	    	num1*=Kd;
	    	double red=I.getRed()*num1;
	    	double green=I.getGreen()*num1;
	    	double blue=I.getBlue()*num1;

	    	//checks that there is no part in the RGB model that is off limits

	    	if(red>255)red=255;
			if(green>255)green=255;
			if(blue>255)blue=255;
	    	return new Color((int)red,(int)green,(int)blue);
	    	
	    }
	    
		public Color calcSpecularComp(double _Ks, Ray v, Vector normal, Vector l, double _nShininess, Color I) {
			//calculates the specular color in a given point according the given formula
			
			double num1=l.dotProduct(normal);
			num1*=2;
			Vector temp=new Vector(normal);
			temp.scale(num1);
			Vector R=new Vector(l);
			R.subtract(temp);//R=D-2(DN)N
			R.normalize();
			num1=-v.get_direction().dotProduct(R);
			if (num1 < 0)
	    		return new Color(0,0,0);
			num1=Math.pow(num1,_nShininess);
			num1*=_Ks;			
			double red=I.getRed()*num1;
	    	double green=I.getGreen()*num1;
	    	double blue=I.getBlue()*num1;
	    	
	    	//checks that there is no part in the RGB model that is off limits
	    	if(red>255)red=255;
			if(green>255)green=255;
			if(blue>255)blue=255;
	    	return new Color((int)red,(int)green,(int)blue);
	    	
		}
		
		private boolean occluded(Light light, Point3D point, Geometry geometry)
		{
			//a boolian function to check if the geometry is occluded by other geometry
			//by using this function we achieve shadows
			
			Vector lightDirection = light.getL(point);
			lightDirection.scale(-1);

			Point3D geometryPoint = new Point3D(point);
			Vector epsVector = new Vector(lightDirection);
			epsVector.scale(2);

			geometryPoint.add(epsVector);
		//	double distance = geometryPoint.distance(((Sphere)geometry).get_center());
			Ray lightRay = new Ray(geometryPoint, lightDirection);
			Map<Geometry, List<Point3D>> intersectionPoints =
			getSceneRayIntersections(lightRay);

			// Flat geometry cannot self intersect
			if (geometry instanceof FlatGeometry){//flat
			 intersectionPoints.remove(geometry);
			}
			//return !intersectionPoints.isEmpty();

			
			for (Entry<Geometry, List<Point3D>> entry: intersectionPoints.entrySet())
				if (entry.getKey().get_material().get_Kt() == 0)
					return true;
				return false;
			
			
		}
		
		
		private Color calcColor(Geometry geometry,Point3D point,Ray inRay) 
		{		
			//function that calls the real calcColor(with 0)
			return calcColor( geometry, point, inRay,0,1.0);
		}
		
		private Ray constructReflectedRay(Vector normal ,Point3D point,Ray inRay)
		{//returns a reflected ray 
			Point3D p=new Point3D(point);
			Vector D=inRay.get_direction();
			D.normalize();
			normal.normalize();
			double num1=D.dotProduct(normal);
			num1*=2;
			Vector temp=new Vector(normal);
			temp.scale(num1);
			Vector R=new Vector(D);
			R.subtract(temp);//R=D-2(DN)N
			R.normalize();
			p.add(R);
			return new Ray(p,R);
		}
		
		private Ray constructRefractedRay(Vector normal ,Point3D point,Ray inRay)
		{//the lecturer said in the lesson to continue the ray from the given point (not to do the calculation with the angles
			Point3D p=new Point3D(point);
			Vector v=new Vector(inRay.get_direction());
			v.normalize();	
			p.add(v);
			return new Ray(p, v);//D from point
		}
		
		private List<Ray> SomeRays(Ray originalRay)
		{
			Vector original=new Vector(originalRay.get_direction());
			original.normalize();//the vector of the original ray normalized
			Vector v1=new Vector(-1*original.getHead().getY().getX(),original.getHead().getX().getX(),0);//the first normal (-y,x,0)
			if(v1.getHead().getX().getX()==0 &&v1.getHead().getY().getX()==0)
				v1=new Vector(1,1,0);
			
			v1.normalize();
			v1.scale(0.05);//the tangens of the angle
			
			///////v1Copy will be the second nornal
			Vector v1Copy=new Vector(v1);//copy of the normal to the original, so it won't change during the process
			v1Copy.crossProduct(original);
			v1Copy.normalize();
			v1Copy.scale(0.05);//the tangens of the angle
			
			//the list to be returned
			List<Ray> lst= new ArrayList<Ray>();
			lst.add(originalRay);
			Vector first=new Vector(original);
			first.add(v1);
			first.normalize();
			lst.add(new Ray(originalRay.get_POO(),first));//original+v1
			Vector second=new Vector(original);
			second.subtract(v1);
			second.normalize();
			lst.add(new Ray(originalRay.get_POO(),second));//original-v1
			Vector third=new Vector(original);
			third.add(v1Copy);
			third.normalize();
			lst.add(new Ray(originalRay.get_POO(),third));//original+v1Copy
			Vector fourth=new Vector(original);
			fourth.subtract(v1Copy);
			fourth.normalize();
			lst.add(new Ray(originalRay.get_POO(),fourth));//original-v1Copy
			
			return lst;

		}
		
		
		private Color calcColor(Geometry geometry,Point3D point,Ray inRay, int level,double k)
		{
			//a function to calculate the color at the given point 
			
			System.out.println(level);
			
			if (level == RECURSION_LEVEL  || k<(1.0/255.0)) return new Color(0, 0, 0);//global RECURSION_LEVEL=3//for the recursion
			
			
			Color ambientLight = _scene.get_ambientLight().getIntensity(point);
			Color emissionLight = geometry.getEmission();
			Color diffuseLight = new Color(0,0,0);
			Color specularLight = new Color(0,0,0);
			int red=ambientLight.getRed() + emissionLight.getRed();
			int green=ambientLight.getGreen() + emissionLight.getGreen();
			int blue=ambientLight.getBlue() + emissionLight.getBlue();
			
			
			
			Iterator<Light> lights = _scene.getLightsIterator();	
			
			while(lights.hasNext())
			{
				Light light=lights.next();
				if (!occluded(light, point, geometry) && inRay.get_direction().dotProduct(geometry.getNormal(point))*light.getL(point).dotProduct(geometry.getNormal(point)) > 0)
				{	
					diffuseLight=(calcDiffusiveComp(geometry.get_material().get_Kd(),geometry.getNormal(point),light.getL(point),light.getIntensity(point)));
					red+=diffuseLight.getRed();
					green+=diffuseLight.getGreen();
					blue+=diffuseLight.getBlue();
			//	diffuseLight=new Color(0,0,0);
				
					Vector v=new Vector(_scene.get_camera().get_P0());
					v.subtract(new Vector(point));
					v.normalize();
					v.scale(-1);
					Ray V=new Ray(point,v);
					specularLight=(calcSpecularComp(geometry.get_material().get_Ks(),V,geometry.getNormal(point),light.getL(point),geometry.get_material().get_n(),light.getIntensity(point)));
					red+=specularLight.getRed();
					green+=specularLight.getGreen();
					blue+=specularLight.getBlue();
				//specularLight=new Color(0,0,0);
				}
				
				Ray reflectedRay = constructReflectedRay(geometry.getNormal(point), point, inRay);
				Map<Geometry, Point3D> reflectedEntry = getClosestPoint(getSceneRayIntersections(reflectedRay));
				Color reflectedColor=Color.BLACK;
				for(Entry<Geometry,Point3D> entry:reflectedEntry.entrySet())
					reflectedColor =calcColor(entry.getKey(),entry.getValue(),reflectedRay, level + 1,k*geometry.get_material().get_Kr());
				
				
				
				//Color reflectedColor = calcColor((reflectedEntry.entrySet()).get, reflectedEntry.entrySet()., reflectedRay, level + 1);
				double kr = geometry.get_material().get_Kr();
				Color reflectedLight = new Color ((reflectedColor.getRed()),( reflectedColor.getGreen()),( reflectedColor.getBlue()));
			
				Ray refractedRay = constructRefractedRay(geometry.getNormal(point), point, inRay);
				List<Ray> Rays=SomeRays(refractedRay);
				double redR=0;
				double greenR=0;
				double blueR=0;
				if(geometry.get_material().get_Kt()>(1.0/255.0))
				{
					for(Ray r:Rays)
					{
					Map<Geometry, Point3D> refractedEntry = getClosestPoint(getSceneRayIntersections(r));
					Color refractedColor=Color.BLACK;
					for(Entry<Geometry,Point3D> entry:refractedEntry.entrySet())
						{refractedColor =calcColor(entry.getKey(),entry.getValue(),r, level + 1,k*geometry.get_material().get_Kt());
				
				
				//Color refractedColor = calcColor(reflectedEntry.geometry, reflectedEntry.point, reflectedRay, level + 1);
						double kt = geometry.get_material().get_Kt();
						Color refractedLight = new Color (( refractedColor.getRed()),( refractedColor.getGreen()),( refractedColor.getBlue()));
						redR+=refractedLight.getRed()*kt;
						greenR+=refractedLight.getGreen()*kt;
						blueR+=refractedLight.getBlue()*kt;
						}
					}
				}
				
				redR/=5;
				greenR/=5;
				blueR/=5;
				red+=(kr *reflectedLight.getRed()+(int)redR);
				green+=(kr *reflectedLight.getGreen()+(int)greenR);
				blue+=(kr *reflectedLight.getBlue()+(int)blueR);
				
						
			}
			
			if(red>255)red=255;
			if(green>255)green=255;
			if(blue>255)blue=255;
			
			Color I0 = new Color (red,green,blue);
			return I0;	
			
		}



}