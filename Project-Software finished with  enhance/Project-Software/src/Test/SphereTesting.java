package Test;
//import Primitives.*;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import Geometries.Sphere;
import Primitives.Coordinate;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;


//import org.junit.jupiter.api.Test;
class SphereTesting {

	@Test
	void SphereTest()
	{
		Sphere t=new Sphere(Color.BLACK,1.0, new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0))/*,new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)))*/);
		Vector n=t.getNormal(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Vector temp=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		assertEquals(n,temp);
	}
	@Test
	void findIntersections()
	{//calculates the intersection points and returns it as a list
		List<Point3D> lst=new ArrayList<Point3D>();
		Vector P0 = new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)));//P0
		Vector O = new Vector((new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-400))));//O
		O.subtract(P0);
		Vector L=O;//L=O-P0
		Vector v=new Vector((new Point3D(new Coordinate(1/Math.sqrt(3)),new Coordinate(-1/Math.sqrt(3)),new Coordinate(-1/Math.sqrt(3)))));//direction
		double tm=L.dotProduct(v);//tm=L*V
		double d=Math.sqrt(Math.pow(L.length(),2)-Math.pow(tm, 2));//d=(L^2-tm^2)^0.5
		//System.out.println(d);
		double _radius=200;
		double th=Math.sqrt(Math.pow(_radius,2)-Math.pow(d,2));
		double t1=tm-th;
		double t2=tm+th;
		if(t1>0)
		{//calculates the point of the intersection
			v.scale(t1);
			Point3D p1=new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
			p1.add(v);
			lst.add(p1);//adds the point to the list
			v.scale(1/t1);//before we multiply it by t1 so now we return it to it's original value
		}
		
		if(t2>0)
		{
			//calculates the point of the intersection
			v.scale(t2);
			Point3D p2=new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
			P0.add(v);
			lst.add(p2);//adds the point to the list	
		}
		Sphere s=new Sphere(Color.BLACK,200.0, new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-400))/*,new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)))*/);
		Ray r=new Ray(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0)),new Vector((new Point3D(new Coordinate(1/Math.sqrt(3)),new Coordinate(-1/Math.sqrt(3)),new Coordinate(-1/Math.sqrt(3))))));

		assertEquals(lst,s.findIntersections(r));
	}
	
	
	@Test
	void intersectionTest2()
	{
		Ray r = new Ray(new Point3D(0,0,0), new Vector(-0.08701766637959463, 0.7951614341583647, -0.6001218371006526));
		Sphere s = new Sphere(Color.BLACK,800,  new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1000)));
		List<Point3D> l = s.findIntersections(r);
		for(Point3D p : l)
			System.out.println(p);
		System.out.print(l.isEmpty());
	}
	


}
