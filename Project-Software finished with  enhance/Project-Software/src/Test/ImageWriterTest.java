package Test;


import org.junit.jupiter.api.Test;

import java.awt.Color;

//import org.junit.Test;

import Renderer.ImageWriter;
//import Renderer.Render;
import Scene.Scene;




class ImageWriterTest {

	@Test
	public void test1()
	{
		
		Scene s=new Scene();
		ImageWriter iw=new ImageWriter("Grid",500,500,50,50);
		s.set_sceneName("t");
		for(int i=0;i<500;i++)
		{
			for(int j=0;j<500;j++)
			{
				if(i%50==0 || j%50==0)
					iw.writePixel(i, j, Color.WHITE);
				else
					iw.writePixel(i, j, Color.BLACK);
			}
		}
		iw.writeToimage();
	}
	
	@Test
	public void test2()
	{
		Scene s=new Scene();
		ImageWriter iw=new ImageWriter("Smiley",500,500,50,50);
		s.set_sceneName("t");
		for(int i=0;i<500;i++)
		{
			for(int j=0;j<500;j++)
			{
				iw.writePixel(i, j, Color.YELLOW);
				if(i>=150 && i<=200)
					if(j>=100 && j<=250)
						iw.writePixel(i, j, Color.BLACK);
				if(i>=300 && i<=350)
					if(j>=100 && j<=250)
						iw.writePixel(i, j, Color.BLACK);
				if(i>=100 && i<=150  || i>=350 && i<=400)
					if(j>=300 && j<=400)
						iw.writePixel(i, j, Color.BLACK);
				if(i>=150 && i<=350)
					if(j>=350 && j<=400)
						iw.writePixel(i, j, Color.BLACK);
			}
		}
		iw.writeToimage();
	}
	
	@Test
	public void test3()
	{
		Scene s=new Scene();
		ImageWriter iw=new ImageWriter("Snake",500,500,50,50);
		s.set_sceneName("t");
		for(int i=0;i<500;i++)
		{
			for(int j=0;j<500;j++)
			{
				iw.writePixel(i, j, new Color(145,225,155));
				//apples
				if((i>=100 && i<=150 && j>=50 && j<=100)||
				(i>=200 && i<=250 && j>=150 && j<=200)||
				(i>=400 && i<=450 && j>=350 && j<=400)||
				(i>=350 && i<=400 && j>=150 && j<=200))
						iw.writePixel(i, j, Color.RED);
				
				if(i%50==0 || j%50==0)
					iw.writePixel(i, j, Color.WHITE);
					
			
				
				//snake
				if((i>=50 && i<=100 && j>=150 && j<=300)||
				(i>=100 && i<=150 && j>=250 && j<=400)||
				(i>=150 && i<=300 && j>=350 && j<=400))
						iw.writePixel(i, j, Color.GREEN);
				//eye
				if(i>=275 && i<=285)
					if(j>=375 && j<=385)
						iw.writePixel(i, j, Color.BLACK);
			}
		}
		iw.writeToimage();
	}
	


}




