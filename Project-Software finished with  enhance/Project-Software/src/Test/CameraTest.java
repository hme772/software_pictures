package Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Elements.Camera;
import Primitives.Coordinate;
import Primitives.Point3D;
import Primitives.Ray;
import Primitives.Vector;

class CameraTest {

	@Test
	void testRaysConstruction() 
	{
		Vector Vin=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));
		Vector Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		Vector Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Camera camera=new Camera(Vup,Vin,Vright);

		//temporary calculating
		Point3D P0 = new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
		double d= 100;
	    Vin=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));
		Vin.scale(d);
		P0.add(Vin);
		Point3D PC= P0;//reference *not* value!!!
		double Rx,Ry=50;
		Rx=Ry;
	    //right calculating
		Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		double X=(3-1.5)*Rx-(Rx/2);
		Vright.scale(X);
		PC.add(Vright);
	    //up calculating
		Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		double Y=((3-1.5)*Ry-(Ry/2));
		Vup.scale(Y);

		PC.subtract(Vup);//the final dot
		Vector V=new Vector(new Point3D(PC.getX(),PC.getY(),PC.getZ()));
		V.normalize();
		Point3D SomeP0 = new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));

		Ray finaly=new Ray(SomeP0,V);
		
		Ray R=camera.constructRayThroughPixel(3, 3, (double) 3, (double) 3, (double) 100, (double) 150,(double) 150);
		System.out.println(R);
		System.out.println(finaly);

		assertEquals(finaly,R);

	}
	
	@Test
	void testRaysConstruction1() 
	{
		//temporary calculating
		Vector Vin=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));
		Vector Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		Vector Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Camera camera=new Camera(Vup,Vin,Vright);
		
		
		Point3D P0 = new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));
		double d= 100;
		 Vin=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));
		Vin.scale(d);
		P0.add(Vin);
		Point3D PC= P0;//reference *not* value!!!
		double Rx,Ry=50;
		Rx=Ry;
	    //right calculating
		 Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		double X=(1-1.5)*Rx-(Rx/2);
		Vright.scale(X);
		PC.add(Vright);
	    //up calculating
		 Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		double Y=((2-1.5)*Ry-(Ry/2));
		Vup.scale(Y);
			

		PC.subtract(Vup);//the final dot
		Vector V=new Vector(new Point3D(PC.getX(),PC.getY(),PC.getZ()));
		V.normalize();
		Point3D SomeP0 = new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(0));

		Ray finaly=new Ray(SomeP0,V);
		

	//	Camera camera=new Camera(Vup,Vin,Vright);
		Ray R=camera.constructRayThroughPixel(3, 3, (double) 1, (double) 2, (double) 100, (double) 150,(double) 150);
		assertEquals(finaly,R);

	}


}
