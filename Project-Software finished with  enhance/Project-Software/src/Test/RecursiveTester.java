//By:Hodaya Elimelech 318627247 && Noa Saadia 208630681


package Test;

import java.awt.Color;

import org.junit.Test;

import Elements.Camera;
import Elements.DirectionalLight;
import Elements.PointLight;
import Elements.SpotLight;
import Geometries.Plane;
import Geometries.Rhombus;
import Geometries.Sphere;
import Geometries.Triangle;
import Primitives.Coordinate;
import Primitives.Material;
import Primitives.Point3D;
import Primitives.Vector;
import Renderer.ImageWriter;
import Renderer.Render;
import Scene.Scene;

public class RecursiveTester {
/*
	@Test
	public void recursiveTest1(){
		
		Scene scene = new Scene();
		scene.set_screenDistance(300);
		
		Sphere sphere = new Sphere(new Color(0, 0, 255), 500, new Point3D(0.0, 0.0, -1000));
		//Sphere sphere2 = new Sphere(Color.RED, 250, new Point3D(0.0, 0.0, -1000));
		//sphere.setEmission(Color.black);
		Material m=new Material();
		m.set_n(200);
		m.set_Kt(1);
		sphere.set_material(m);
		scene.addGeometry(sphere);


		
		//Sphere sphere2 = new Sphere(Color.BLUE, 250, new Point3D(0.0, 0.0, -1000));
		Sphere sphere2 = new Sphere(new Color(20, 20, 20), 250, new Point3D(0.0, 0.0, -1000));
		Material m2=new Material();
		m2.set_Kd(1);
		m2.set_n(35);
		m2.set_Kt(0);
		sphere2.set_material(m2);
	scene.addGeometry(sphere2);
	Triangle triangle = new Triangle(new Color (0, 0, 100),
			 new Point3D(-125, -225, -260),
			 new Point3D(-225, -125, -260),
			 new Point3D(-225, -225, -270)
		);

//	Sphere sphere2 = new Sphere (new Color(0,0,100),50, new Point3D(-175,-175,-260));

		Material m1=new Material();
		m1.set_n(4);
		m1.set_Kd(1);
		m1.set_Ks(1);
		triangle.set_material(m1);
		//scene.addGeometry(triangle);
	
	
		scene.addLight(new SpotLight(new Color(255, 100, 100), new Point3D(-200, -200, -150), 
					   1, 0.00001, 0.0000005,  new Vector(2, 2, -3)));
	
		ImageWriter imageWriter = new ImageWriter("Recursive Test1 KUKU", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		imageWriter.writeToimage();
	}
	
	
	
	@Test
	public void recursiveTest2(){
		
		Scene scene = new Scene();
		scene.set_screenDistance(300);
		
		Sphere sphere = new Sphere( new Color(0,0,100), 300, new Point3D(-550, -500, -1000));
		
		sphere.get_material().set_n(20);
		
		
		
		sphere.get_material().set_Kt(0.5);
		
		scene.addGeometry(sphere);
		
		Sphere sphere2 = new Sphere(new Color(100, 20, 20), 150, new Point3D(-550, -500, -1000));
		sphere2.get_material().set_n(20);
		
		
		
		sphere2.get_material().set_Kt(0);
		
	
		
	
		scene.addGeometry(sphere2);
		
		Triangle triangle = new Triangle(new Color(20, 20, 20), new Point3D(  1500, -1500, -1500),
				 						 new Point3D( -1500,  1500, -1500),
				 						 new Point3D(  200,  200, -375));
		
		Triangle triangle2 = new Triangle(new Color(20, 20, 20), new Point3D(  1500, -1500, -1500),
										  new Point3D( -1500,  1500, -1500),
										  new Point3D( -1500, -1500, -1500));
		
		
		triangle.get_material().set_Kr(1);
		triangle2.get_material().set_Kr(0.5);
		
		
		scene.addGeometry(triangle);
		scene.addGeometry(triangle2);

		
		scene.addLight(new SpotLight(new Color(255, 100, 100),  new Point3D(200, 200, -150), 
				   1, 0.000001, 0.0000005,  new Vector(-2, -2, -3)));
	
		
		
		
		ImageWriter imageWriter = new ImageWriter("65Recursive Test 2 kuku", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		imageWriter.writeToimage();
		
	}
	
	@Test
	public void recursiveTest3(){
		
		Scene scene = new Scene();
		//scene.set_screenDistance(300);
		
		Sphere sphere = new Sphere(new Color(0, 0, 100), 300, new Point3D(0, 0, -1000));
		Material sphere1Mat = new Material();
		sphere1Mat.set_Kt(0.5);
		sphere1Mat.set_n(20);
		sphere.set_material(sphere1Mat);
		
		
		
		scene.addGeometry(sphere);
		
		Sphere sphere2 = new Sphere(new Color(100, 20, 20), 150, new Point3D(0, 0, -1000));
		
		
		Material sphereMat = new Material();
		sphereMat.set_Kt(0);
		sphereMat.set_n(20);
		sphere2.set_material(sphereMat);
	
		scene.addGeometry(sphere2);
		
		Triangle triangle = new Triangle(new Color(20, 20, 20),
				new Point3D(  2000, -1000, -1500),
				 						 new Point3D( -1000,  2000, -1500),
				 						 new Point3D(  700,  700, -375));
		
		Triangle triangle2 = new Triangle(new Color(20, 20, 20), new Point3D(  2000, -1000, -1500),
										  new Point3D( -1000,  2000, -1500),
										  new Point3D( -1000, -1000, -1500));
		
		Material m1 = new Material();
		Material m2 = new Material();
		m1.set_Kr(1);
		m2.set_Kr(0.5);
		triangle.set_material(m1);
		triangle2.set_material(m2);
		
		
		scene.addGeometry(triangle);
		scene.addGeometry(triangle2);

		scene.addLight(new SpotLight(new Color(255, 100, 100),  new Point3D(200, 200, -150), 
				  1, 0.00001, 0.000005,   new Vector(-2, -2, -3)));
	
		ImageWriter imageWriter = new ImageWriter("Recursive Test 3 kuku", 500, 500, 500, 500);
		
		Render render = new Render(scene, imageWriter);
		
		render.renderImage();
		imageWriter.writeToimage();
		
	}
	@Test
	public void recursiveTest4(){

	Scene scene = new Scene();
	//scene.set_screenDistance(300);
	
	Sphere sphere = new Sphere(new Color(0, 0, 100), 300, new Point3D(0, 0, -1000));
	Material sphere1Mat = new Material();
	sphere1Mat.set_Kt(0.5);
	sphere1Mat.set_n(20);
	sphere.set_material(sphere1Mat);
	
	
	
	scene.addGeometry(sphere);
	
	Sphere sphere2 = new Sphere(new Color(100, 20, 20), 150, new Point3D(0, 0, -1000));
	
	
	Material sphereMat = new Material();
	sphereMat.set_Kt(0);
	sphereMat.set_n(20);
	sphere2.set_material(sphereMat);

	scene.addGeometry(sphere2);
	scene.addLight(new SpotLight(new Color(255, 100, 100),  new Point3D(200, 200, -150), 
			  1, 0.00001, 0.000005,   new Vector(-2, -2, -3)));
	
	Triangle triangle2 = new Triangle(new Color(20, 20, 20), new Point3D(  2000, -1000, -1500),
			  new Point3D( -1000,  2000, -1500),
			  new Point3D( -1000, -1000, -1500));
	
	triangle2.set_material(sphereMat);
	
	
	scene.addGeometry(triangle2);

	ImageWriter imageWriter = new ImageWriter("Recursive Test 4 kuku", 500, 500, 500, 500);
	
	Render render = new Render(scene, imageWriter);
	
	render.renderImage();
	imageWriter.writeToimage();
	}
	
	/*
	@Test
	public void recursiveTestFinal()
	{
		Scene scene = new Scene();
		Vector Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		Vector Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Vector Vto=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));

		Camera c=new Camera(Vup,Vto,Vright);
		c.set_P0(new Point3D(0,0,50));//0,0,50
		scene.set_camera(c);
		//scene.set_background(new Color(145,225,155));
		scene.set_screenDistance(100);
		
		
		///////////////green background and a stage
		Plane background=new Plane(new Vector(0,0,-1),new Point3D(0,0,-1760),new Color(0,183,12));
		Material pMaterial=new Material();
		pMaterial.set_Kd(1);
		pMaterial.set_Kr(0.00001);
		pMaterial.set_Ks(1);
		pMaterial.set_Kt(0.00001);
		
		background.set_material(pMaterial);
		scene.addGeometry(background);
		
		Rhombus stage=new Rhombus(new Color(115,67,4),new Vector(20000,0,0),new Vector(-6500,-2500,900),new Point3D(-7800, -2500, -1700));
		

		
		stage.set_material(pMaterial);
		scene.addGeometry(stage);
		//////////////
		
		
		//hands and legs
		Sphere right_arm = new Sphere(new Color(100, 20, 20), 170, new Point3D(480, -750, -690));
		right_arm.get_material().set_n(20);
		
		right_arm.get_material().set_Kt(0);
		scene.addGeometry(right_arm);
		
		Sphere left_arm = new Sphere(new Color(100, 20, 20), 170, new Point3D(-480, -750, -690));
		left_arm.get_material().set_n(20);
		
		left_arm.get_material().set_Kt(0);
		scene.addGeometry(left_arm);
		
		
		
		
		Sphere right_leg = new Sphere(new Color(100, 20, 20), 160, new Point3D(250, -1600, -690));
		right_leg.get_material().set_n(20);
		
		right_leg.get_material().set_Kt(0);
		scene.addGeometry(right_leg);
		
		Sphere left_leg = new Sphere(new Color(100, 20, 20), 160, new Point3D(-250, -1600, -690));
		left_leg.get_material().set_n(20);
		
		left_leg.get_material().set_Kt(0);
		scene.addGeometry(left_leg);
		//////
		
		
		//head and body
		Sphere head = new Sphere(new Color(100, 20, 20), 500, new Point3D(0, 0, -700));
		head.get_material().set_n(20);
		
		head.get_material().set_Kt(0);
		scene.addGeometry(head);
		
		Sphere body = new Sphere(new Color(100, 20, 20), 400, new Point3D(0, -750, -700));
		body.get_material().set_n(20);
		
		body.get_material().set_Kt(0);
		scene.addGeometry(body);
		///
		
	
		
		//mouth = transparent so will not cause a wierd shadow next to the nose
		Triangle mouth=new Triangle(Color.black,new Point3D(0,-80,-200),new Point3D(40,-110,-200),new Point3D(-40,-110,-200));
		mouth.get_material().set_n(20);
		
		mouth.get_material().set_Kt(1);
		scene.addGeometry(mouth);
		//ears
		Sphere left_ear = new Sphere(new Color(100, 20, 20), 150, new Point3D(-550, 550, -700));
		left_ear.get_material().set_n(20);
		
		left_ear.get_material().set_Kt(0);
		scene.addGeometry(left_ear);
		
		Sphere right_ear = new Sphere(new Color(100, 20, 20), 150, new Point3D(550, 550, -700));
		right_ear.get_material().set_n(20);
		
		right_ear.get_material().set_Kt(0);
		scene.addGeometry(right_ear);
		//eyes(there will be 2 lights so they will glow)
		Sphere left_eye = new Sphere(new Color(0, 0, 0), 50, new Point3D(-100, 50, -200));
		left_eye.get_material().set_n(20);
		
		//left_eye.get_material().set_n(50);
		left_eye.get_material().set_Kr(0.5);
		left_eye.get_material().set_Kt(0.2);
		left_eye.get_material().set_Kd(0.3);

		scene.addGeometry(left_eye);
		
		
		Sphere right_eye = new Sphere(new Color(0, 0, 0), 50, new Point3D(100, 50, -200));
		right_eye.get_material().set_n(50);
		
		//right_eye.get_material().set_n(50);
		right_eye.get_material().set_Kr(0.5);
		right_eye.get_material().set_Kt(0.2);
		
		right_eye.get_material().set_Kd(0.3);
		scene.addGeometry(right_eye);
		//nose
		Sphere nose = new Sphere(new Color(0, 0, 0), 20, new Point3D(0,-50,-200));
		nose.get_material().set_n(20);
		
		//nose.get_material().set_n(50);
		//nose.get_material().set_Kr(0.5);
		nose.get_material().set_Kt(0);
		scene.addGeometry(nose);
		
		
		//we've build the mirror on the air(it is not close to the stage at all
		Rhombus mirror=new Rhombus(new Color(40,40,40),new Vector(-200,0,500),new Vector(200,-2500,0),new Point3D(-1500,800,-1000));
		Material m=new Material();
		m.set_Kt(0.01);
		m.set_Kr(0.99999);
		m.set_Kd(0.00001);
		m.set_Ks(1);
		mirror.set_material(m);
		scene.addGeometry(mirror);
		
		Material hatMat = new Material();
		hatMat.set_Kt(0);
		hatMat.set_n(20);

		Triangle hat = new Triangle(Color.yellow, new Point3D(  500, 400, -700),
				  new Point3D( 0,  1200, -700),
				  new Point3D(  -500, 400, -700));

		
		
		hat.set_material(hatMat);
	
		
		scene.addGeometry(hat);
/////////////////////////////////////
		//a lollipop
		Sphere sphere = new Sphere( new Color(0,0,150), 60, new Point3D(650, -50, -200));
		
		sphere.get_material().set_n(20);
		sphere.get_material().set_Kd(0.3);
		//sphere.setEmission(emission);
		
		
		sphere.get_material().set_Kt(0.5);
		
		scene.addGeometry(sphere);
		
		Sphere sphere2 = new Sphere(new Color(100, 20, 20), 40,new Point3D(650, -50, -200));
		sphere2.get_material().set_n(20);
	//	sphere2.get_material().set_Kd(0.3);

		
		
		sphere2.get_material().set_Kt(0);
		Rhombus r=new Rhombus(new Color(158,152,146),new Vector(50,0,0),new Vector(-400,-200,0),new Point3D(580, -80, -200));

		
		
		
	
		scene.addGeometry(sphere2);
		scene.addGeometry(r);
		
		scene.addLight((new PointLight(new Color(200,200,200), new Point3D(100, 50, -175),//right eye, 
				   1, 0.001, 0.0005)));
		scene.addLight((new PointLight(new Color(200,200,200), new Point3D(-100, 50, -175),//left eye
				   1, 0.001, 0.0005)));
		
		
		scene.addLight((new PointLight(new Color(100,100,100), new Point3D(7500, 5500, 0),//rigth spotlight created by using a point light  
				   1, 0.00000000001, 0.0000000005)));
		scene.addLight((new PointLight(new Color(100,100,100), new Point3D(-7500, 5500, 0),//rigth spotlight created by using a point light
				   1, 0.00000000001, 0.0000000005)));
		
		ImageWriter imageWriter = new ImageWriter("Recursive Test final", 500, 500, 500, 500);
		Render render = new Render(scene, imageWriter);
		render.renderImage();
		imageWriter.writeToimage();
	}
	*/
	@Test
	public void recursiveTestFinalWithEnhance()
	{
		Scene scene = new Scene();
		Vector Vright=new Vector(new Point3D(new Coordinate(1),new Coordinate(0),new Coordinate(0)));
		Vector Vup=new Vector(new Point3D(new Coordinate(0),new Coordinate(1),new Coordinate(0)));
		Vector Vto=new Vector(new Point3D(new Coordinate(0),new Coordinate(0),new Coordinate(-1)));

		Camera c=new Camera(Vup,Vto,Vright);
		c.set_P0(new Point3D(0,0,50));//0,0,50
		scene.set_camera(c);
		//scene.set_background(new Color(145,225,155));
		scene.set_screenDistance(100);
		
		
		///////////////green background and a stage
		Plane background=new Plane(new Vector(0,0,-1),new Point3D(0,0,-1760),new Color(0,183,12));
		Material pMaterial=new Material();
		pMaterial.set_Kd(1);
		pMaterial.set_Kr(0.00001);
		pMaterial.set_Ks(1);
		pMaterial.set_Kt(0);
		
		background.set_material(pMaterial);
		scene.addGeometry(background);
		
		Rhombus stage=new Rhombus(new Color(115,67,4),new Vector(20000,0,0),new Vector(-6500,-2500,900),new Point3D(-7800, -2500, -1700));
		

		
		stage.set_material(pMaterial);
		scene.addGeometry(stage);
		//////////////
		
		
		//hands and legs
		Sphere right_arm = new Sphere(new Color(100, 20, 20), 170, new Point3D(480, -750, -690));
		right_arm.get_material().set_n(20);
		
		right_arm.get_material().set_Kt(0);
		scene.addGeometry(right_arm);
		
		Sphere left_arm = new Sphere(new Color(100, 20, 20), 170, new Point3D(-480, -750, -690));
		left_arm.get_material().set_n(20);
		
		left_arm.get_material().set_Kt(0);
		scene.addGeometry(left_arm);
		
		
		
		
		Sphere right_leg = new Sphere(new Color(100, 20, 20), 160, new Point3D(250, -1600, -690));
		right_leg.get_material().set_n(20);
		
		right_leg.get_material().set_Kt(0);
		scene.addGeometry(right_leg);
		
		Sphere left_leg = new Sphere(new Color(100, 20, 20), 160, new Point3D(-250, -1600, -690));
		left_leg.get_material().set_n(20);
		
		left_leg.get_material().set_Kt(0);
		scene.addGeometry(left_leg);
		//////
		
		
		//head and body
		Sphere head = new Sphere(new Color(100, 20, 20), 500, new Point3D(0, 0, -700));
		head.get_material().set_n(20);
		
		head.get_material().set_Kt(0);
		scene.addGeometry(head);
		
		Sphere body = new Sphere(new Color(100, 20, 20), 400, new Point3D(0, -750, -700));
		body.get_material().set_n(20);
		
		body.get_material().set_Kt(0);
		scene.addGeometry(body);
		///
		
	
		
		//mouth = transparent so will not cause a wierd shadow next to the nose
		Triangle mouth=new Triangle(Color.black,new Point3D(0,-80,-200),new Point3D(40,-110,-200),new Point3D(-40,-110,-200));
		mouth.get_material().set_n(20);
		
		mouth.get_material().set_Kt(1);
		scene.addGeometry(mouth);
		//ears
		Sphere left_ear = new Sphere(new Color(100, 20, 20), 150, new Point3D(-550, 550, -700));
		left_ear.get_material().set_n(20);
		
		left_ear.get_material().set_Kt(0);
		scene.addGeometry(left_ear);
		
		Sphere right_ear = new Sphere(new Color(100, 20, 20), 150, new Point3D(550, 550, -700));
		right_ear.get_material().set_n(20);
		
		right_ear.get_material().set_Kt(0);
		scene.addGeometry(right_ear);
		//eyes(there will be 2 lights so they will glow)
		Sphere left_eye = new Sphere(new Color(0, 0, 0), 50, new Point3D(-100, 50, -200));
		left_eye.get_material().set_n(20);
		
		//left_eye.get_material().set_n(50);
		left_eye.get_material().set_Kr(0.5);
		left_eye.get_material().set_Kt(0.2);
		left_eye.get_material().set_Kd(0.3);

		scene.addGeometry(left_eye);
		
		
		Sphere right_eye = new Sphere(new Color(0, 0, 0), 50, new Point3D(100, 50, -200));
		right_eye.get_material().set_n(50);
		
		//right_eye.get_material().set_n(50);
		right_eye.get_material().set_Kr(0.5);
		right_eye.get_material().set_Kt(0.2);
		
		right_eye.get_material().set_Kd(0.3);
		scene.addGeometry(right_eye);
		//nose
		Sphere nose = new Sphere(new Color(0, 0, 0), 20, new Point3D(0,-50,-200));
		nose.get_material().set_n(20);
		
		//nose.get_material().set_n(50);
		//nose.get_material().set_Kr(0.5);
		nose.get_material().set_Kt(0);
		scene.addGeometry(nose);
		
		
		//we've build the mirror on the air(it is not close to the stage at all
		Rhombus mirror=new Rhombus(new Color(0,0,0),new Vector(-200,0,500),new Vector(200,-2500,0),new Point3D(-1500,800,-1000));
		Material m=new Material();
		m.set_Kt(0.15);
		m.set_Kr(0.99999);
		m.set_Kd(0.00001);
		m.set_Ks(1);
		mirror.set_material(m);
		scene.addGeometry(mirror);
		
		Material hatMat = new Material();
		hatMat.set_Kt(0);
		hatMat.set_n(20);

		Triangle hat = new Triangle(Color.yellow, new Point3D(  500, 400, -700),
				  new Point3D( 0,  1200, -700),
				  new Point3D(  -500, 400, -700));

		
		
		hat.set_material(hatMat);
	
		
		scene.addGeometry(hat);
/////////////////////////////////////
		//a lollipop
		Sphere sphere = new Sphere( new Color(0,0,150), 60, new Point3D(650, -50, -200));
		
		sphere.get_material().set_n(20);
		sphere.get_material().set_Kd(0.3);
		//sphere.setEmission(emission);
		
		
		sphere.get_material().set_Kt(0.5);
		
		scene.addGeometry(sphere);
		
		Sphere sphere2 = new Sphere(new Color(100, 20, 20), 40,new Point3D(650, -50, -200));
		sphere2.get_material().set_n(20);
		sphere2.get_material().set_Kd(0.3);
		
		
		sphere2.get_material().set_Kt(0);
		Rhombus r=new Rhombus(new Color(158,152,146),new Vector(50,0,0),new Vector(-400,-200,0),new Point3D(580, -80, -200));

		
		Triangle glass = new Triangle(new Color(0,0,0), new Point3D(  1550, -1200, -150),
				  new Point3D( 0,  1200, -150),
				  new Point3D(  0, -1200, -150));

		glass.get_material().set_Kd(0);
		glass.get_material().set_Kr(0);
		glass.get_material().set_Ks(0);
		glass.get_material().set_Kt(0.5);
		
		
		scene.addGeometry(glass);
		scene.addGeometry(sphere2);
		scene.addGeometry(r);
		
		scene.addLight((new PointLight(new Color(200,200,200), new Point3D(100, 50, -175),//right eye, 
				   1, 0.001, 0.0005)));
		scene.addLight((new PointLight(new Color(200,200,200), new Point3D(-100, 50, -175),//left eye
				   1, 0.001, 0.0005)));
		
		
		scene.addLight((new PointLight(new Color(100,100,100), new Point3D(7500, 5500, 0),//rigth spotlight created by using a point light  
				   1, 0.00000000001, 0.0000000005)));
		scene.addLight((new PointLight(new Color(100,100,100), new Point3D(-7500, 5500, 0),//rigth spotlight created by using a point light
				   1, 0.00000000001, 0.0000000005)));
		
		ImageWriter imageWriter = new ImageWriter("Recursive Test final with enhance", 500, 500, 500, 500);
		Render render = new Render(scene, imageWriter);
		render.renderImage();
		imageWriter.writeToimage();
	}
	
	 
}
	
	
	



