package Elements;

import java.awt.Color;
//import java.util.Objects;

import Primitives.Point3D;
import Primitives.Vector;

public class SpotLight extends PointLight
{
	//a light that is like point light but also has a direction
	Vector _direction;

	//constructor
	public SpotLight(Color _color, Point3D _position, double _kc, double _kl, double _kq, Vector _direction) {
		super(new Color(_color.getRed(),_color.getGreen(),_color.getBlue()),new Point3D( _position), _kc, _kl, _kq);
		this._direction =new Vector( _direction);
	}
	//gets and sets values

	public Vector get_direction() {
		return new Vector(_direction);
	}

	public void set_direction(Vector _direction) {
		this._direction = new Vector(_direction);
	}

	
	//administration
	
	@Override
	public String toString() {
		return "SpotLight [_direction=" + _direction + "]";
	}

	@Override
	public boolean equals(Object obj) {
		
		SpotLight p =(SpotLight)obj;
		return ( p.get_kc()==this.get_kc()&&p.get_kl()==this.get_kl()&&p.get_kq()==this.get_kq()&&p._direction.equals(this._direction));
		}
	
	

	//operations
	@Override
	public Color getIntensity(Point3D point) {
		//calculates the color at this point

		Vector directionTemp=new Vector(get_direction());
		directionTemp.normalize();
		Color basicColor= super.getIntensity(point);//pointlight calculation
		
		double result=directionTemp.dotProduct(this.getL(point));
		if (result < 0) result = 0;//so the color will not be less than 0
		int red= (int)Math.round(basicColor.getRed()*result);
		int green = (int)Math.round( basicColor.getGreen()*result);
		int blue = (int)Math.round( basicColor.getBlue()*result);
		
		//checks that the color is within the range(will not cross 255 in each parameter)

		if(red>255)red=255;
		if(green>255)green=255;
		if(blue>255)blue=255; 
		Color I0 = new Color (red,green,blue);
		return I0;
	}

	
	
	@Override
	public Vector getL(Point3D point) 
	{
		///vector l from the light source to the point
		Vector l=new Vector(_position);
		l.subtract(new Vector(point));
		l.scale(-1);
		l.normalize();
		return l;
	}

}
