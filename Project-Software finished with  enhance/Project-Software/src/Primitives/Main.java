//phase 1 
//By:Hodaya Elimelech 318627247 && Noa Saadia 208630681
//all the primitives


package Primitives;

public class Main {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		Vector v1=new Vector(new Point3D(new Coordinate(1),new Coordinate(2),new Coordinate(3)));
		Vector v2=new Vector(new Point3D(new Coordinate(2),new Coordinate(4),new Coordinate(6)));
		System.out.println("checks placement in vector:"+'\n'+v1);
		System.out.println(v2);
		Vector v3=v1.crossProduct(v2);
		System.out.println("checks cross product:"+'\n'+v3);
		v1.normalize();
		System.out.println("checks normalize:"+'\n'+v1);
		v2.normalize();
		System.out.println(v2);
		v1.scale(2);
		System.out.println("checks scale:"+'\n'+v1);
		v2.scale(0.5);
		System.out.println(v2);
		v1.add(v2);
		System.out.println("checks add:"+'\n'+v1);
		v1.subtract(v2);
		System.out.println("checks subtract:"+'\n'+v1);
		System.out.println("checks length:"+'\n'+v1.length());
		System.out.println("checks dot product:"+'\n'+v1.dotProduct(v2));
		
		
	}

}
/*Output:
 * checks placement in vector:
Vector (Coordinate [x=1.0],Coordinate [x=2.0],Coordinate [x=3.0])
Vector (Coordinate [x=2.0],Coordinate [x=4.0],Coordinate [x=6.0])
checks cross product:
Vector (Coordinate [x=0.0],Coordinate [x=0.0],Coordinate [x=0.0])
checks normalize:
Vector (Coordinate [x=0.2672612419124244],Coordinate [x=0.5345224838248488],Coordinate [x=0.8017837257372732])
Vector (Coordinate [x=0.2672612419124244],Coordinate [x=0.5345224838248488],Coordinate [x=0.8017837257372732])
checks scale:
Vector (Coordinate [x=0.5345224838248488],Coordinate [x=1.0690449676496976],Coordinate [x=1.6035674514745464])
Vector (Coordinate [x=0.1336306209562122],Coordinate [x=0.2672612419124244],Coordinate [x=0.4008918628686366])
checks add:
Vector (Coordinate [x=0.6681531047810609],Coordinate [x=1.3363062095621219],Coordinate [x=2.004459314343183])
checks subtract:
Vector (Coordinate [x=0.5345224838248488],Coordinate [x=1.0690449676496976],Coordinate [x=1.6035674514745462])
checks length:
1.9999999999999998
checks dot product:
1.0
*/




