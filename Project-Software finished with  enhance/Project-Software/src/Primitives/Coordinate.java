package Primitives;

//import java.util.Objects;

public class Coordinate
{
	double x;
//constructor
	public Coordinate(double x) 
	{
		//x.super();
		this.x = x;
	}
	//gets and sets values
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}
	

	//administration
	@Override
	public boolean equals(Object obj)
	{
		if ( obj instanceof Coordinate)
		{
			return(((Coordinate)obj).x==this.x);	 
		}
		return false;
	}
	@Override
	public String toString() {
	return "Coordinate [x=" + x + "]";
}
	//operations
	public void add(double _x)
	{
		this.x+=_x;
	}
	
	public void subtract (double _x)
	{
		this.x-=_x;
	}

}
